import { Request, Response } from 'express';
import { createUser } from '@services/ValidationServices';
import { defaultResCatchService } from '@services/defaultResCatchService';
import { ToastUtils } from '@utils/Toast';
import { Token } from '@utils/Token'
import {getUserByEmailServices} from "@services/UserServices";

class AuthController {
    async register(req: Request, res: Response): Promise<void> {
        try {
            const { email, password } = req.body;

            // const candidate = await getUserByEmailServices(email);
            //
            // if (candidate) {
            //     return ToastUtils.sendToastWithError(res, 'Регистрация', 'Такой пользователь уже существует');
            // }

            const codeActive = Math.floor(Math.random() * 9999) + 1000;
            const registerUser = await createUser(email, password, codeActive);

            if (!registerUser) {
                return ToastUtils.sendToastWithError(res, 'Регистрация', 'Ошибка регистрации');
            }

            // const token = Token.getToken({ user_id: registerUser._id, email: registerUser.email });
            // ToastUtils.sendToastWithSuccess(res, 'Регистрация', 'Вы успешно прошли регистрацию', { token });
        } catch (error) {
            defaultResCatchService(error, res);
        }
    }

    async login(req: Request, res: Response): Promise<void> {
        try {
            const { email, password } = req.body;

            const candidate = await getUserByEmailServices(email);

            if (!candidate)
                return ToastUtils.sendToastWithError(res, 'Авторизация', 'Такого пользователя не существует');

            if (candidate.email !== email || candidate.password !== password)
                return ToastUtils.sendToastWithError(res, 'Авторизация', 'Неверная пара Email/пароль');

            const token = Token.getToken({ user_id: candidate._id, email: candidate.email });
            ToastUtils.sendToastWithSuccess(res, 'Авторизация', 'Вы успешно авторизовались', { token });
        } catch (error) {
            defaultResCatchService(error, res);
        }
    }

    async getMe(req: Request, res: Response): Promise<void> {
        try {
            const token = req.body.token as string;

            if (!token) {
                return ToastUtils.sendToastWithError(res, 'Система', 'Системная ошибка');
            }

            const decodedToken = Token.verifyToken(token)
            const user_id = decodedToken.user_id;

            res.status(200).json({ user_id });
        } catch (error) {
            defaultResCatchService(error, res);
        }
    }
}

export default new AuthController();
