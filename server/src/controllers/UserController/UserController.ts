import { Request, Response } from 'express';
import { defaultResCatchService } from '@services/defaultResCatchService';
import { ToastUtils } from '@utils/Toast';
import {
  getUserByEmailServices,
  getUserByIdServices,
  getUsers,
  updateUserByIdServices,
  updateUserImage
} from "@services/UserServices";
import {UserUpdateProps} from "@models/UserModel";
import {Token} from "@utils/Token";

class ProductController {

  async getAllUsers(req: Request, res: Response) {
    try {
      const users = await getUsers();

      res.status(200).json({ users });
    } catch (error) {
      defaultResCatchService(error, res);
    }
  }

  async getUserById(req: Request, res: Response) {
    try {
      const id = req.query._id as string;

      if (!id)
        return ToastUtils.sendToastWithError(res, 'Система', 'Неверный идентификатор пользователя');

      const user = await getUserByIdServices(id);

      if (!user)
        return ToastUtils.sendToastWithError(res, 'Система', 'Такого пользователя не существует');

      res.status(200).json({ user });
    } catch (error) {
      defaultResCatchService(error, res);
    }
  }

  async getUserByEmail(req: Request, res: Response): Promise<void> {
    try {
      const email = req.query.email as string;

      if (!email) {
        return ToastUtils.sendToastWithError(res, 'Система', 'Неверный идентификатор пользователя');
      }

      const user = await getUserByEmailServices(email);

      if (!user)
        return ToastUtils.sendToastWithError(res, 'Система', 'Такого пользователя не существует');

      res.status(200).json({ user });
    } catch (error) {
      defaultResCatchService(error, res);
    }
  }

  async getProfile(req: Request, res: Response): Promise<void> {
    try {
      const user_id = req.params.user_id as string;


      if (!user_id)
        return ToastUtils.sendToastWithError(res, 'Система', 'Неверный идентификатор пользователя');

      const user = await getUserByIdServices(user_id);

      if (!user)
        return ToastUtils.sendToastWithError(res, 'Система', 'Такого пользователя не существует');

      res.status(200).json({ user });
    } catch (error) {
      defaultResCatchService(error, res);
    }
  }

  async updateProfile(req: Request, res: Response): Promise<void> {
    try {
      const user_id = req.params.user_id as string;
      const data: UserUpdateProps = req.body;

      if (!user_id)
        return ToastUtils.sendToastWithError(res, 'Система', 'Неверный идентификатор пользователя');

      const userFind = await getUserByIdServices(user_id);

      if (!userFind)
        return ToastUtils.sendToastWithError(res, 'Система', 'Такого пользователя не существует');

      const user = await updateUserByIdServices(user_id, data)

      res.status(200).json({ user });
    } catch (error) {
      defaultResCatchService(error, res);
    }
  }

  async updateImage(req: Request, res: Response): Promise<void> {
    try {
      const file = req.file;
      const token = req.headers.authorization?.split(' ')[1]
      const decodedToken = Token.verifyToken(token ?? '')
      const user_id = decodedToken.user_id;

      if (!file) return ToastUtils.sendToastWithError(res, 'Загрузка изображения', 'Произошла ошибка, попробуйте добавить другой файл')

      await updateUserImage(user_id, file.filename)

      ToastUtils.sendToastWithSuccess(
        res,
        'Загрузка изображения',
        'Вы успешно загрузили новое изображение',
        { avatar: file.filename }
      )
    } catch (error) {
      defaultResCatchService(error, res);
    }
  }
}

export default new ProductController();
