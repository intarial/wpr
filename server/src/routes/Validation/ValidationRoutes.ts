import {Router} from 'express'
import AuthController from "@controllers/AuthController/AuthController";

const router = Router()

router.post('/register', AuthController.register)
router.post('/login', AuthController.login)
router.post(`/me`, AuthController.getMe)

export default router
