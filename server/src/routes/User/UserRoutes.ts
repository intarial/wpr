import {Router} from 'express'
import UserController from "@controllers/UserController/UserController";
import AuthMiddleware from "../../middlewares/authMiddleware";
import {imageUtils} from "@utils/Image";

const router = Router()

const baseUrlUpdate = '/update'

router.get(`/all`, UserController.getAllUsers)
router.get(`/by-id`, UserController.getUserById)
router.get(`/by-email`, UserController.getUserByEmail)
router.get(`/:user_id`, UserController.getProfile)
router.put(`${baseUrlUpdate}/profile/:user_id`, AuthMiddleware, UserController.updateProfile)
router.put(`${baseUrlUpdate}/password/:user_id`, AuthMiddleware, UserController.getProfile)
router.put(`${baseUrlUpdate}/image`, imageUtils.uploadProfile.single('image'), AuthMiddleware, UserController.updateImage)

export default router