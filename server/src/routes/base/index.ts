import {Router} from 'express'
import UserRoutes from "@routes/User/UserRoutes";
import ValidateRoutes from "@routes/Validation/ValidationRoutes";

const router = Router()

router.use('/validate', ValidateRoutes)
router.use('/user', UserRoutes)
export default router
