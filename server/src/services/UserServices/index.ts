import UserModel, {UserUpdateProps} from "@models/UserModel";

export const getUsers = async () => {
  const user = await UserModel.find()
  return user
}

export const getUserByEmailServices = async (email: string) => {
  const user = await UserModel.findOne({email})
  return user
}

export const getUserByIdServices = async (_id: string) => {
  const user = await UserModel.findOne({ _id })
  return user
}

export const updateUserByIdServices = async (_id: string, body: UserUpdateProps) => {
  const options = { new: true, useFindAndModify: false };
  const user = await UserModel.findByIdAndUpdate(_id, body, options);
  return user;
};

export const updateUserImage = async (_id: string, image: string) => {
  const user = await UserModel.findByIdAndUpdate(_id, { avatar: image });
  return user;
};
