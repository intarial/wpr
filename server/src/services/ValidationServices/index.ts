import UserModel from "@models/UserModel";
import {getConnection} from "../../index";

export const createUser = async (email: string, password: string, activeCode: number) => {
  const connection = await getConnection();

  try {
    const query = `INSERT INTO users (email, password, activeCode) VALUES (?, ?, ?)`;
    const values = [email, password, activeCode];
    const [rows] = await connection.execute(query, values);

    console.log(rows)

    return rows;
  } catch (error) {
    throw new Error("Failed to create user");
  } finally {
    connection.end();
  }
}

export const activateUser = async (email: string) => {
  const user = await UserModel.updateOne({email}, {isActive: true})
  return user
}
