import {Response} from 'express'

export const defaultResCatchService = (e: unknown, res: Response) => {
  console.log(e)
  res.status(500).json({
    toast: { title: 'Система', description: 'Непредвиденная ошибка', type: 'error' }
  })
}