import {NextFunction, Response} from 'express'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()

export default (req: any, res: Response, next: NextFunction) => {
    if (req.method === 'OPTIONS') {
        return next()
    }
    try {
        const token = req.headers.authorization?.split(' ')[1]
        if (!token) {
            return res.status(401).json({
                toast: {
                    description: 'Нет авторизации! Status 401',
                    type: 'error'
                }
            })
        }
        req.user = jwt.verify(token, process.env.JWT_KEY ?? '')
        next()
    } catch (e) {
        res.status(401).json({
            toast: {
                description: 'Нет авторизации! Status 401',
                type: 'error'
            }
        })
    }
}