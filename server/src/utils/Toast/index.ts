import { Response } from 'express';

interface ToastData {
  title: string;
  description: string;
  status: 'success' | 'error' | 'info' | 'warning';
}

const sendToast = (res: Response, data: ToastData, statusCode: number, additionalData?: Record<string, unknown>) => {
  res.status(statusCode).json({
    toast: data,
    ...additionalData
  });
};

const sendToastWithError = (res: Response, title: string, description: string, additionalData?: Record<string, unknown>) => {
  sendToast(res, { title, description, status: 'error' }, 401, additionalData);
};

const sendToastWithSuccess = (res: Response, title: string, description: string, additionalData?: Record<string, unknown>) => {
  sendToast(res, { title, description, status: 'success' }, 200, additionalData);
};

export const ToastUtils = { sendToastWithError, sendToastWithSuccess };