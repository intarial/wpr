import dotenv from 'dotenv'
import jwt from "jsonwebtoken";

dotenv.config()

const getToken = (data: {}) => jwt.sign({ ...data }, process.env.JWT_KEY ?? '')
const verifyToken = (token: string) => jwt.verify(token, process.env.JWT_KEY ?? '') as { user_id: string }

export const Token = { getToken, verifyToken }