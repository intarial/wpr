import multer from 'multer';
import path from 'path';
import fs from 'fs';

const uploadDirProfile = path.join(path.resolve(__dirname, '..', '..'), 'uploads', 'images', 'profile');

if (!fs.existsSync(uploadDirProfile)) {
  fs.mkdirSync(uploadDirProfile, { recursive: true });
}

const storageProfile = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadDirProfile);
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const ext = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + ext);
  }
});

const uploadProfile = multer({ storage: storageProfile });

export const imageUtils = { uploadProfile };