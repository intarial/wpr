import mongoose, {Schema, Document, model} from 'mongoose'

export interface IUser extends Document {
    _id: string
    email: string
    password: string
    firstName?: string
    lastName?: string
}

export interface UserUpdateProps {
    firstName?: string
    lastName?: string
    email?: string
    password?: string
}

const UserSchema = new Schema({
    email: {type: String, required: true, unique: true},
    firstName: {type: String, required: false, unique: false},
    lastName: {type: String, required: false, unique: false},
    password: {type: String, required: true},
})

export default model<IUser>('User', UserSchema)
