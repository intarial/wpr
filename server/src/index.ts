import 'module-alias/register';
import express from 'express';
import path from "path";
import router from '@routes/base/index'
import dotenv from 'dotenv'
import {Connection, createConnection} from 'mysql2/promise';
import cors from 'cors'

export const getConnection = async (): Promise<Connection> => {
  return createConnection({
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
  });
};

dotenv.config()

const app = express();

const PORT = process.env.PORT;
const MYSQL_HOST = process.env.MYSQL_HOST;
const MYSQL_USER = process.env.MYSQL_USER;
const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD;
const MYSQL_DATABASE = process.env.MYSQL_DATABASE;

const corsOptions = {
  exposedHeaders: 'Films-Total-Count',
};

app.use(cors(corsOptions))
app.use(express.json())
app.use('/api', router)
app.use('/images', express.static(path.join(__dirname, '..', 'build', 'uploads', 'images')));

const start = async () => {
  try {
    if (!MYSQL_HOST || !MYSQL_USER || !MYSQL_DATABASE) {
      return console.log('Настройте .env файл');
    }

    const connection = getConnection()

    console.log(`MySQL: [WPR-SERVER] Started!`);

    app.listen(PORT, () => console.log(`SERVER: [${PORT}]  Started!`));
  } catch (e) {
    console.log(`Error>>Index>>Start`, e);
    process.exit(1);
  }
};

start();
