import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import path from 'path';

// https://vitejs.dev/config/

export default defineConfig({
  publicDir: 'http://62.84.101.126:3333/',
  server: {
    host: '0.0.0.0',
    port: 80,
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@assets': path.resolve(__dirname, './src/assets'),
      '@utils': path.resolve(__dirname, './src/utils'),
      '@constants': path.resolve(__dirname, './src/constants'),
      '@pages': path.resolve(__dirname, './src/pages'),
      '@ui': path.resolve(__dirname, './src/ui'),
      '@routes': path.resolve(__dirname, './src/routes'),
      '@packages': path.resolve(__dirname, './src/packages'),
      '@components': path.resolve(__dirname, './src/components'),
    },
  },
  plugins: [react()],
})
