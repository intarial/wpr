import {
  createStandaloneToast,
  extendTheme,
  withDefaultColorScheme,
} from "@chakra-ui/react";
import { colors } from "./colors";
import { components } from "./components";

export const theme = extendTheme(
  {
    colors,
    components,
    styles: {
      global: {
        "html, body": {
          overflowX: "hidden",
          minH: "100vh",
        },
        ".leaflet-container": {
          rounded: "lg",
          borderWidth: 1,
          zIndex: 0,
        },
        ".markdown-preview": {
          wordBreak: "break-word",
          whiteSpace: "pre-wrap",
          a: {
            color: "blue.400",
            textDecor: "underline",
          },
          color: "slate.600",
          fontSize: "lg",
          lineHeight: "lg",
        },
        ".markdown-preview-news": {
          wordBreak: "break-word",
          whiteSpace: "pre-wrap",
          a: {
            color: "blue.400",
            textDecor: "underline",
          },
          color: "slate.600",
          fontSize: "md",
          lineHeight: "md",
        },
      },
    },
  },
  withDefaultColorScheme({ colorScheme: "red" })
);

export const { ToastContainer, toast } = createStandaloneToast({
  defaultOptions: {
    position: "top-right",
  },
  theme,
});
