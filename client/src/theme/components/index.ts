import { buttonTheme } from "./button";
import { inputTheme } from "./input";
import { tableTheme } from "./table";
import { tabsTheme } from "./tabs";
import { textareaTheme } from "./textarea";

export const components = {
  Button: buttonTheme,
  Input: inputTheme,
  Tabs: tabsTheme,
  Textarea: textareaTheme,
  Table: tableTheme,
};
