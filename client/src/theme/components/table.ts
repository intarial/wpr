import { tableAnatomy as parts } from "@chakra-ui/anatomy";
import { createMultiStyleConfigHelpers } from "@chakra-ui/styled-system";
import { mode } from "@chakra-ui/theme-tools";

const { defineMultiStyleConfig, definePartsStyle } =
  createMultiStyleConfigHelpers(parts.keys);

const variantSimple = definePartsStyle((props) => {
  return {
    thead: {
      bg: mode("slate.50", "slate.800")(props),
    },
    th: {
      color: mode("gray.600", "gray.400")(props),
      bg: mode("slate.50", "slate.800")(props),
      borderBottom: "1px",
      borderColor: mode("slate.200", "slate.700")(props),
      p: 3,
      _first: {
        roundedTopLeft: "md",
      },
      _last: {
        roundedTopRight: "md",
      },
    },
    td: {
      borderBottom: "1px",
      p: 3,
      borderColor: mode(`slate.100`, `slate.700`)(props),
    },
    tr: {
      _last: {
        td: {
          borderBottomWidth: 0,
        },
      },
    },
    caption: {
      color: mode("gray.600", "gray.100")(props),
    },
    tfoot: {
      tr: {
        "&:last-of-type": {
          th: { borderBottomWidth: 0 },
        },
      },
    },
    table: {
      borderWidth: 1,
      p: 0,
      borderRadius: "lg",
      borderCollapse: "separate",
      borderSpacing: 0,
      borderColor: mode("slate.200", "slate.800")(props),
    },
  };
});

const variants = {
  simple: variantSimple,
};

export const tableTheme = defineMultiStyleConfig({
  variants,
  defaultProps: {
    variant: "simple",
    size: "sm",
    colorScheme: "gray",
  },
});
