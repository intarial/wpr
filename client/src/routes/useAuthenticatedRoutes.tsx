import {Navigate, useRoutes} from "react-router-dom";
import {Profile} from "@pages/profile/Profile.tsx";
import {Landing} from "@pages/landing/Landing.tsx";
import {useAppSelector} from "@packages/store/hook.ts";
import {ProfileSettings} from "@pages/profile-settings/ProfileSettings.tsx";
import {CourseTest} from "@pages/course-test/CourseTest.tsx";
import {ConstructorPage} from "@pages/constructor-page/ConstructorPage.tsx";
// import { MainLayout } from "../components/layouts/MainLayout";

export const useAuthenticatedRoutes = () => {
  const user = useAppSelector((store) => store.user);
  const basePathUserProfile = "/profile/:id";
  const basePathPageConstructor = "/construct/:router";
  const basePathMyProfile = "/profile";
  const basePathCourse = "/course";

  return useRoutes([
    {
      path: basePathUserProfile,
      element: <Profile />
    },
    {
      path: `${basePathPageConstructor}`,
      element: <ConstructorPage />
    },
    {
      path: `${basePathMyProfile}-settings`,
      element: <ProfileSettings />,
    },
    {
      path: `${basePathCourse}-test`,
      element: <CourseTest />,
    },
    {
      path: "/landing",
      element: <Landing/>
    },
    {
      path: "*",
      element: <Navigate to={`/profile/${user.user._id}`} replace />,
    },
  ]);
};
