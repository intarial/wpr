export enum ROUTE_NAMES  {
  LANDING = '/landing',

  VALIDATION = '/validation',
  VALIDATION_LOGIN = '/validate/login',
  VALIDATION_REGISTER = '/validate/register',

  PROFILE = '/profile',
  PROFILE_SETTINGS = '/profile-settings',
}
