import {ROUTE_NAMES} from "./const.ts";
import {NavigateOptions} from "react-router-dom";
import {Validation} from "@pages/validation/Validation.tsx";
import {Landing} from "@pages/landing/Landing.tsx";

interface Route {
  name: ROUTE_NAMES;
  component: () => JSX.Element;
  options?: NavigateOptions;
}

const authRoutes: Route[] = [
  {
    name: ROUTE_NAMES.VALIDATION_LOGIN,
    component: Validation,
  },
  {
    name: ROUTE_NAMES.VALIDATION_REGISTER,
    component: Validation,
  }
];

const landingRoutes: Route[] = [
  {
    name: ROUTE_NAMES.LANDING,
    component: Landing,
  }
];

export const routes: Route[] = [
  ...authRoutes,
  ...landingRoutes,
];