import { Navigate, useRoutes } from "react-router-dom";
import {Landing} from "@pages/landing/Landing";
import {Validation} from "@pages/validation/Validation.tsx";

export const useUnauthenticatedRoutes = () => {
  return useRoutes([
    {
      path: "landing",
      element: <Landing />
    },
    {
      path: "validation",
      element: <Validation />,
    },
    {
      path: "*",
      element: <Navigate to="/landing" replace />,
    },
  ]);
};
