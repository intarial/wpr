import {IUser} from "@packages/api/typings/user.ts";

const getNameInFirstLast = (user: IUser) => {
  return `${ user.firstName ?? 'Анонимный' } ${ user.lastName ?? 'пользователь' }`
}

  export const UserUtils = { getNameInFirstLast }