const month = (num: number) => {
  const cases = [2, 0, 1, 1, 1, 2];
  const titles = ['месяц', 'месяца', 'месяцев'];
  return titles[(num % 100 > 4 && num % 100 < 20) ? 2 : cases[Math.min(num % 10, 5)]];
}

export const DeclensionsUtils = { month }