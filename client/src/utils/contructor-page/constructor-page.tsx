import {TPageConstructorsTypes} from "@packages/api/typings/page.ts";
import {Constructor} from "@pages/constructor-page/components/constructor/Constructor.tsx";
import {
  ICardsPageConstructor, IJobOpeningsPageConstructor,
  IListPageConstructor,
  IPageConstructor, IPricesPageConstructor,
  ISliderPageConstructor
} from "@packages/api/typings/page-constructor.ts";
import {LearningPathways} from "@pages/landing/components/learning-pathways/LearningPathways.tsx";
import {Reviews} from "@components/reviews/Reviews.tsx";
import {CodeologyMaterials} from "@pages/landing/components/codeology-materials/CodeologyMaterials.tsx";

const getConstructorByLabel = (label: TPageConstructorsTypes, content: IPageConstructor) => {
  switch (label) {
    case "slider": {
      if (content as IPageConstructor<'slider'>) {
        const sliderContent = content.values as ISliderPageConstructor[];
        return <Constructor.Slider content={sliderContent} title={ content.title } />;
      }
    } break;
    case "list": {
      if (content as IPageConstructor<'list'>) {
        const listContent = content.values as IListPageConstructor[];
        return <Constructor.List content={listContent} title={ content.title } description={ content.description } />;
      }
    } break;
    case "prices": {
      if (content as IPageConstructor<'prices'>) {
        const priceContent = content.values as IPricesPageConstructor[];
        return <Constructor.Prices content={priceContent[0]} title={ content.title } description={ content.description } />;
      }
    } break;
    case "cards": {
      if (content as IPageConstructor<'cards'>) {
        const cardsContent = content.values as ICardsPageConstructor[];
        return <Constructor.Cards content={cardsContent} title={ content.title } />;
      }
    } break;
    case "job_openings": {
      if (content as IPageConstructor<'job_openings'>) {
        const cardsContent = content.values as IJobOpeningsPageConstructor[];
        return <Constructor.JobOpenings content={cardsContent} title={ content.title } />;
      }
    } break;
    case "pathways": {
      if (content as IPageConstructor<'pathways'>) {
        return <LearningPathways />;
      }
    } break;
    case "reviews": {
      if (content as IPageConstructor<'reviews'>) {
        return <Reviews />;
      }
    } break;
    case "materials": {
      if (content as IPageConstructor<'materials'>) {
        return <CodeologyMaterials />;
      }
    } break;
    default: return null;
  }
};

export const ConstructorPageUtils = { getConstructorByLabel }