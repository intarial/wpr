import {useQuery} from "@tanstack/react-query";
import {queryKeys} from "@packages/api/queryKeys.ts";
import api from "@packages/api";
import {useAppDispatch} from "@packages/store/hook.ts";
import {setCoursesReduce} from "@packages/store/slices/courses.ts";

export const Initialization = () => {
  const dispatch = useAppDispatch()

  useQuery(
    [queryKeys.getAllCourses],
    () => api.courseApi.getAllCourses(),
    {
      onSuccess: (req) => {
        dispatch(setCoursesReduce(req.data.courses));
      },
    }
  );
}