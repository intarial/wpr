import {time} from "@utils/date/time.ts";
import {date} from "@utils/date/date.ts";

export const dateUtils = { time, date }