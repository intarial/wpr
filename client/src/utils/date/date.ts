import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

const weeksLengthToMonth = (weeks: number) => {
  return weeks / 4;
};

const transcriptionDate = (date: string, formatStr: 'd MMMM') => {
  return format(new Date(date), formatStr, { locale: ru });
};

export const date = { weeksLengthToMonth, transcriptionDate }