const getTimerInDate = (dateString: string) => {
  const targetDate = new Date(dateString);
  const timeDiff = targetDate.getTime() - Date.now();

  if (timeDiff < 0) {
    return null;
  }

  const hours = Math.floor(timeDiff / (1000 * 60 * 60));
  const minutes = Math.floor((timeDiff / (1000 * 60)) % 60);
  const seconds = Math.floor((timeDiff / 1000) % 60);

  return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
};

export const time = { getTimerInDate }