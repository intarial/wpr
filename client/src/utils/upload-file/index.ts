import api from "@packages/api";

const profile = async (file: FileList) => {
  return await api.userApi.uploadProfileImage(file)
};

export const UploadFile = { profile }