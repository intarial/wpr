import { useAuthenticatedRoutes } from "./routes/useAuthenticatedRoutes";
import { useUnauthenticatedRoutes } from "./routes/useUnauthenticatedRoutes";
import {Initialization} from "@utils/initialization.tsx";
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {authenticateUser} from "@packages/api/client/httpClient.ts";
import {useEffect, useState} from "react";
import Cookies from "js-cookie";
import {ACCESS_TOKEN} from "@constants/cookies.ts";
import {setUsersReduce} from "@packages/store/slices/user/user.ts";
import {useToast} from "@chakra-ui/react";
import {Loader} from "@ui/Loader/Loader.tsx";

function Codeology() {
  const dispatch = useAppDispatch()
  const toast = useToast()

  const user = useAppSelector((store) => store.user);

  const [isLoading, setIsLoading] = useState(true);
  const isLoggedIn = user.isLoginIn;
  const authenticatedRoutes = useAuthenticatedRoutes();
  const unauthenticatedRoutes = useUnauthenticatedRoutes();

  useEffect(() => {
    const accessToken = Cookies.get(ACCESS_TOKEN);
    if (accessToken) {
      setIsLoading(true);
      authenticateUser(accessToken)
        .then((res) => {
          dispatch(setUsersReduce({ user: {...res.user, courses: []}}))
          setIsLoading(false)
        })
        .catch(err => {

          setIsLoading(false)
          toast({ ...err.response.data.toast })
        })
    } else {
      setIsLoading(false)
    }
  }, []);

  Initialization()

  if (isLoading) return <Loader />

  return (
    <>
      {isLoggedIn ? authenticatedRoutes : unauthenticatedRoutes}
    </>
  );
}

export default Codeology;
