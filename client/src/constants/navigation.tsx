const footer = [
  {
    title: 'Направления',
    label: 'directions',
    urls: [
      {
        title: 'Python разработчик',
        path: '/'
      },
      {
        title: 'Frontend Java Script разработчик',
        path: '/'
      },
      {
        title: 'Backend Java Script разработчик',
        path: '/'
      },
      {
        title: 'Fullstack Java Script разработчик',
        path: '/'
      },
      {
        title: 'C# разработчик',
        path: '/'
      },
      {
        title: 'Java разработчик',
        path: '/'
      }
    ]
  },
  {
    title: 'Проекты',
    label: 'projects',
    urls: [
      {
        title: 'Медиа',
        path: '/'
      },
      {
        title: 'Распродажа',
        path: '/'
      }
    ]
  },
  {
    title: 'О Кодологии',
    label: 'information',
    urls: [
      {
        title: 'О нас',
        path: '/'
      },
      {
        title: 'Отзывы',
        path: '/'
      },
      {
        title: 'Контакты',
        path: '/'
      },
      {
        title: 'Вакансии',
        path: '/'
      }
    ]
  }
]

const documents = [
  {
    title: 'Сведения об образовательной организации',
    path: '/'
  },
  {
    title: 'Политика конфиденциальности',
    path: '/'
  },
  {
    title: 'Оферта',
    path: '/'
  }
]

export const navigation = { footer, documents }