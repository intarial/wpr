import {IconTelegram, IconVK, IconYouTube} from "@assets/icons";

export const socials = [
  {
    name: {
      en: 'VKontakte',
      ru: 'ВКонтакте'
    },
    icon: <IconVK />,
    path: 'vk.com'
  },
  {
    name: {
      en: 'YouTube',
      ru: 'YouTube'
    },
    icon: <IconYouTube />,
    path: 'youtube.com'
  },
  {
    name: {
      en: 'Telegram',
      ru: 'Телеграм'
    },
    icon: <IconTelegram />,
    path: 'telegram.com'
  },
]