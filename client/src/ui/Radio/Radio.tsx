import React from 'react';
import styles from './Radio.module.scss';

export const Radio = ({ active }: { active: boolean }) => {
    return (
        <div className={ styles.wrapper } >
          <div className={[ active ? styles.active : styles.deactivated ].join(' ')} />
        </div>
    )
}