import React from 'react';
import styles from './NotProfile.module.scss';
import {NavLink} from "react-router-dom";
import {ROUTE_NAMES} from "@routes/const.ts";
import {Button} from "@ui/Button/Button.tsx";

export const NotProfile: React.FC = () => {

    return (
      <div className={ styles.switcher }>
        <p className={ styles.textSwitch }>
          Еще нет профиля?
        </p>
        <NavLink
          state={{ type: 'register' }}
          to={ ROUTE_NAMES.VALIDATION }
        >
          <Button
            size={ 'small' }
            text={ 'Зарегистрироваться' }
            color={ 'blue' }
          />
        </NavLink>
      </div>
    )
}