import React from 'react';
import styles from './Button.module.scss';
import {IButton} from "@ui/ui.types";
import {ChevronDown} from "lucide-react";
import {Tooltip} from "@chakra-ui/react";

export const Button: React.FC<IButton> = (
  {
    color = 'light',
    size = 'medium',
    type,
    arrow,
    icon,
    text,
    hover = 'blue',
    disabled,
    style,
    onClick,
    tooltip
  }) => {
  const buttonClasses = [styles.wrapper, styles[color]];

  if (size === 'small') buttonClasses.push(styles.smallButton);
  else buttonClasses.push(styles.mediumButton);

  if (hover === 'orange' || color === 'blue') buttonClasses.push(styles.orangeHover)
  if (disabled) buttonClasses.push(styles.disabled)

  if (arrow?.direction === 'right') buttonClasses.push(styles.arrowRightDirection)
  if (arrow?.direction === 'top') buttonClasses.push(styles.arrowTopDirection)
  if (arrow?.direction === 'left') buttonClasses.push(styles.arrowLeftDirection)

  return (
      //@ts-ignore
      <div style={ ...style }>
        <Tooltip openDelay={ 500 } label={ tooltip }>
          <button
            disabled={ disabled }
            type={ type }
            onClick={ () => onClick && onClick() }
            className={buttonClasses.join(' ')}>
            {arrow?.position === 'left' && <ChevronDown />}
            {icon ?? ''}
            {text && <p className={styles.text}>{text}</p>}
            {arrow?.position === 'right' && <ChevronDown />}
          </button>
        </Tooltip>
      </div>
  );
};