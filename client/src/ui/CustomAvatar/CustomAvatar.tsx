import {Avatar} from "@chakra-ui/avatar";
import {randomColor} from "@chakra-ui/theme-tools";
import {ResponsiveValue} from "@chakra-ui/react";
import styles from "./CustomAvatar.module.scss";

interface ICustomAvatar {
    name?: string,
    url?: string,
    size?: ResponsiveValue<"sm" | "md" | "lg" | "xl" | "2xl" | string | "2xs" | "xs" | "full">,
    className?: string
    width?: 'x50' | 'x100' | 'x120'
}

export const CustomAvatar= ({ name, url, size, className, width }: ICustomAvatar) => {
    const colors = ["green.500", "red.500", "red.200", "yellow.200", "yellow.300", 'green.300', 'green.100'];

    const classNames = [className]
    width && classNames.push(styles[width])

    return <Avatar
      bgColor={randomColor({ colors })}
      className={ classNames.join(' ') }
      name={ name }
      src={ url }
      size={ size }
    />
}