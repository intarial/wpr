import {Swiper, SwiperSlide} from "swiper/react";

interface ICustomSwiper {
  items: any[],
  render: (item: any) => JSX.Element
  slidesPerView?: number
  spaceBetween?: number
}

export const CustomSwiper = ({ items, render, slidesPerView, spaceBetween }: ICustomSwiper) => {
  if (!items || typeof render !== 'function') return null;

  return (
    <div>
      <Swiper
        slidesPerView={ slidesPerView ?? 3 }
        spaceBetween={ spaceBetween ?? 20 }
        pagination={{
          clickable: true,
        }}
        className="mySwiper"
      >
        {
          items.map(
            (item, index) => (
              <SwiperSlide key={ index }>
                { render(item) }
              </SwiperSlide>
            )
          )
        }
      </Swiper>
    </div>
  )
}