import styles from './CustomInput.module.scss';
import { IInput } from "@ui/ui.types.ts";
import { Input } from "@chakra-ui/react";

export const CustomInput = ({ props }: { props: IInput }) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        props.change(event.target.value);
    }

    return (
      <Input
        placeholder={props.placeholder}
        className={styles.wrapper}
        value={props.value}
        onChange={handleChange}
        type={props?.type}
      />
    )
}