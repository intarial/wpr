import {Spinner, Stack} from "@chakra-ui/react";

export const Loader = () => {
  return (
    <Stack
      w={ '100vw' }
      height={ '100vh' }
      alignItems={ 'center' }
      justifyContent={ 'center' }
      direction='row'
      spacing={4}
    >
      <Spinner size='xl' color='blue.500' />
    </Stack>
  );
}