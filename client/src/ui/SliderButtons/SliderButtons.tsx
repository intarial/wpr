import styles from './SldierButtons.module.scss';
import {Button} from "@ui/Button/Button.tsx";
import {ChevronLeft} from "lucide-react";

export interface ISliderButtons {
  activeValue: number
  countValue: number
  updateValue: (type: 'minus' | 'plus') => void
  buttons?: {
    color?: 'dark' | 'grey' | 'light' | 'blue' | 'white',
    textNext?: string
    textPrev?: string
    completeText?: string
    outText?: string
  }
}

export const SliderButtons = ({ activeValue, updateValue, countValue, buttons } : ISliderButtons): JSX.Element => (

  <div className={styles.buttonsInteractive}>
    <Button
      text={activeValue === 1 ? buttons?.outText ?? 'Вернуться' : buttons?.textPrev ?? ''}
      size='small'
      color={ buttons?.color }
      icon={<ChevronLeft />}
      onClick={() => updateValue('minus')}
    />
    <p className={styles.pageCounter}>
      {activeValue} / {countValue ?? 1}
    </p>
    <Button
      disabled={ activeValue === undefined }
      text={activeValue === countValue ? buttons?.completeText ?? 'Завершить' : buttons?.textNext ?? 'Дальше'}
      size='small'
      color={ buttons?.color }
      onClick={() => updateValue('plus')}
      arrow={{ position: 'right', direction: 'right' }}
    />
  </div>
);