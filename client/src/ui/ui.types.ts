import {StyleProps} from "@chakra-ui/react";

export interface IButton {
  size: 'small' | 'medium'
  onClick?: () => void
  color?: 'dark' | 'grey' | 'light' | 'blue' | 'white'
  style?: StyleProps
  br?: number
  active?: boolean
  text?: string
  disabled?: boolean
  arrow?: {
    position: 'right' | 'left'
    direction?: 'right' | 'top' | 'down' | 'left'
  }
  hover?: 'blue' | 'orange'
  icon?: JSX.Element
  tooltip?: string
  type?: 'submit'
}

export interface IInput {
  value: string | number
  change: (val: string) => void
  type?: 'text' | 'number' | 'password'
  placeholder?: string
  searchContent?: {
    label: string
  }
}

export interface IToast {
  title: string
  description: string
  isClosable?: boolean
  type: "success" | "error" | "warning" | "info" | "loading" | undefined
}

export interface IToastResponse {
  title: string
  description: string
  status: "success" | "error" | "warning" | "info" | "loading" | undefined
}