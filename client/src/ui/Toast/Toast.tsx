
import {IToast} from "@ui/ui.types.ts";
import {useToast} from "@chakra-ui/react";

export const CustomToast = ({ type, description, title, isClosable = true }: IToast) => {
  const toast = useToast()

    return (
      toast({
        title,
        description,
        status: type,
        isClosable
      })
    )
}

export const toastResponse = (toast?: IToast) => {

}