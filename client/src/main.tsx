import React from 'react'
import ReactDOM from 'react-dom/client'
import Codeology from './Codeology.tsx'
import '@assets/styles/index.css'
import {BrowserRouter} from "react-router-dom";
import {queryClient} from "@packages/lib/query-client";
import { QueryClientProvider } from "@tanstack/react-query";
import {Provider} from "react-redux";
import store from "@packages/store/store.ts";
import { theme, ToastContainer } from "./theme";
import {ChakraBaseProvider} from '@chakra-ui/react'
import {ImageModalProvider} from "@components/image/image-modal/ImageModal.tsx";

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <QueryClientProvider client={queryClient}>
        <ChakraBaseProvider theme={ theme }>
          <Provider store={ store }>
            <Codeology />
            <ImageModalProvider />
          </Provider>
        </ChakraBaseProvider>
      </QueryClientProvider>
      <ToastContainer />
    </BrowserRouter>
  </React.StrictMode>,
)
