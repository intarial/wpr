import styles from './ProfileSettings.module.scss';
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {MainLayout} from "@components/layouts/main-layout/MainLayout.tsx";
import {ProfileUserSide} from "@components/profile/ProfileUserSide.tsx";
import {Button} from "@ui/Button/Button.tsx";
import CustomFormSettingsProfile from "./components/custom-form-settings-profile/CustomForm.tsx";
import * as Yup from 'yup'
import api from "@packages/api";
import {setUsersReduce} from "@packages/store/slices/user/user.ts";

const validationProfileSchema = Yup.object({
  lastName: Yup.string().required('Обязательное поле'),
  firstName: Yup.string().required('Обязательное поле'),
  birthday: Yup.string(), phone: Yup.string(), address: Yup.string()
});

const validationPasswordSchema = Yup.object({
  password: Yup.string().min(8, 'Минимум 8 латинских символов и цифр').max(25, 'Максимум 25 символов').required('Обязательное поле'),
  newPassword: Yup.string().min(8, 'Минимум 8 латинских символов и цифр').max(25, 'Максимум 25 символов').required('Обязательное поле'),
  newRePassword: Yup.string().min(8, 'Минимум 8 латинских символов и цифр').max(25, 'Максимум 25 символов').required('Обязательное поле')
});

export const ProfileSettings = () => {
  const { user } = useAppSelector((store) => store);
  const dispatch = useAppDispatch()

  const initialValuesProfile = {
    lastName: user.user?.lastName ?? '',
    firstName: user.user?.firstName ?? '',
    birthday: user.user?.birthday ?? '',
    phone: user.user?.phone ?? '',
    address: user.user?.address ?? ''
  };

  const initialValuesPassword = {
    password: '',
    newPassword: '',
    newRePassword: '',
  };

  const handleSubmitProfile = (val: {[p: string]: string}) => {
    api.userApi.updateProfile({
      _id: user.user._id,
      ...val,
    })
      .then((res) => {
        console.log(res)
        dispatch(setUsersReduce({ user: res.data.user }))
      })
  }

  const handleSubmitPassword = (val: {[p: string]: string}) => {
    api.userApi.updatePassword({
      _id: user.user._id,
      ...val,
    })
  }

  return (
    <MainLayout>
      <div className={ styles.profileContent }>
        <div className={styles.headerContent}>
          <ProfileUserSide
            change
            user={ user.user }
          />
          <Button size="small" text="Выйти из профиля"/>
        </div>
        <CustomFormSettingsProfile
          validationSchema={ validationProfileSchema }
          initialValues={ initialValuesProfile }
          submitIsActive={ true }
          title={ 'О себе' }
          description={ 'Вносите реальные данные, потому что они будут отображены в сертификате' }
          onSubmit={ (val) => handleSubmitProfile(val) }
        />
        <CustomFormSettingsProfile
          validationSchema={ validationPasswordSchema }
          initialValues={ initialValuesPassword }
          submitIsActive={ true }
          title={ 'Пароль' }
          description={ 'Вносите реальные данные, потому что они будут отображены в сертификате' }
          onSubmit={ (val) => handleSubmitPassword(val) }
        />
      </div>
    </MainLayout>
  );
};

