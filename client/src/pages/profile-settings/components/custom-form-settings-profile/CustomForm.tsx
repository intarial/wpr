import React from 'react';
import styles from './CustomForm.module.scss'
import { Formik, Form, Field, FormikProps } from 'formik';
import * as Yup from 'yup';
import {AnimatePresence, motion} from "framer-motion";
import {Button} from "@ui/Button/Button.tsx";

interface CustomFormProps {
  initialValues: {
    [key: string]: string;
  };
  validationSchema: Yup.ObjectSchema<Record<string, unknown>>;
  onSubmit: (values: { [key: string]: string }, actions: any) => void;
  title?: string
  description?: string
  submitIsActive?: boolean
}

interface FormikCreateProps {
  initialValues: {
    [key: string]: string;
  };
  validationSchema: Yup.ObjectSchema<Record<string, unknown>>;
  onSubmitWrapper?: any
  submitIsActive?: boolean
}

const initialLabelsProfile: { [key: string]: string; } = {
  firstName: 'Имя',
  lastName: 'Фамилия',
  birthday: 'Дата рождения',
  phone: 'Номер телефона',
  address: 'Адрес'
}

const initialPlaceholdersProfile: { [key: string]: string; } = {
  firstName: 'Ваше имя',
  lastName: 'Ваша фамилия',
  birthday: 'Ваша дата рождения',
  phone: 'Ваш номер телефона',
  address: 'Ваш адрес',
  password: 'Текущий пароль',
  newPassword: 'Новый пароль',
  newRePassword: 'Повторите пароль',
}

export const FormikCreate: React.FC<FormikCreateProps> = ({ initialValues, validationSchema, onSubmitWrapper, submitIsActive = false }) => {
  return (
    <Formik initialValues={initialValues} onSubmit={onSubmitWrapper} validationSchema={validationSchema}>
      {(props: FormikProps<{ [key: string]: string }>) => (
        <Form className={ styles.listFields }>
          {Object.keys(props.values).map((key) => (
            <div className={ styles.fieldBlock } key={key}>
              {
                initialLabelsProfile[key] &&  (
                  <label htmlFor={key}>{initialLabelsProfile[key]}</label>
                )
              }
              <Field
                placeholder={ initialPlaceholdersProfile[key] }
                className={ styles.field }
                type="text"
                name={key}
                id={key}
              />
              <AnimatePresence>
                {props.touched[key] && props.errors[key] && (
                  <motion.div
                    initial={{
                      height: 10
                    }}
                    animate={{ height: 'auto' }}
                    className={ styles.error }
                  >
                    <div>{props.errors[key]}</div>
                  </motion.div>
                )}
              </AnimatePresence>
            </div>
          ))}
          {
            submitIsActive &&
              <Button
                  text={ 'Сохранить' }
                  size={ 'medium' }
                  type="submit"
                  disabled={props.isSubmitting}
              />
          }
        </Form>
      )}
    </Formik>
  )
}

const CustomFormSettingsProfile: React.FC<CustomFormProps> = ({ title, description, initialValues, validationSchema, onSubmit, submitIsActive }) => {
  const onSubmitWrapper = (values: { [key: string]: string }, actions: any) => {
    onSubmit(values, actions);
  };

  return (
    <div className={ styles.wrapper }>
      <div className={ styles.textContent }>
        <h4 className={ styles.title }>{ title } </h4>
        <p className={ styles.description }>{ description }</p>
      </div>
      <FormikCreate
        onSubmitWrapper={ onSubmitWrapper }
        initialValues={ initialValues }
        submitIsActive={ submitIsActive }
        validationSchema={ validationSchema }
      />
    </div>
  );
};

export default CustomFormSettingsProfile;