import {Slider} from "./components/Slider/Slider.tsx";
import { List } from "./components/List/List.tsx";
import {Prices} from "./components/Prices/Prices.tsx";
import { Cards } from "./components/Cards/Cards.tsx";
import {JobOpenings} from "@pages/constructor-page/components/constructor/components/JobOpenings/JobOpenings.tsx";

export const Constructor = {
  Slider,
  Prices,
  Cards,
  JobOpenings,
  List
}