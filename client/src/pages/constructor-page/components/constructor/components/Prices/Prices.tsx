
import styles from './Prices.module.scss';
import {IPricesPageConstructor} from "@packages/api/typings/page-constructor.ts";
import {CustomInput} from "@ui/CustomInput/CustomInput.tsx";
import {Button} from "@ui/Button/Button.tsx";
import {useState} from "react";
import {date} from "@utils/date/date.ts";
import {Brief} from "@components/brief/Brief.tsx";

const PriceBlock = ({ installment, price, action } : {  price: number, installment?: number, action?: number }) => {
  price = Math.floor(installment ? price / installment : price)

  return (
    <div className={[ styles.priceBlock, installment && styles.priceBlockInstallment ].join(' ')}>
      <p className={ styles.text }>{ installment ? 'Частями без переплат' : 'Полная стоимость' }</p>
      <div className={ styles.moneyComplex }>
        {
          action && <p className={ styles.actionPrice }>
            { price?.toLocaleString('ru') } ₽{ installment &&  '/мес' }
            </p>
        }
        <p className={ styles.mainPrice }>
          {
            Math.floor(
              action ? price - (price / 100 * action)
                : price
            )
              ?.toLocaleString('ru')
          } <span>₽{ installment &&  '/мес'}</span>
        </p>
      </div>
    </div>
  )
}

export const Prices = ({ content, title, description } : { content: IPricesPageConstructor, title: string, description?: string }) => {
  const [promo, setPromo] = useState('')

  return (
    <div className={ styles.wrapper }>
      <div className={ styles.contentPrices }>
        <div className={ styles.startTag }>
          <div className={ styles.content }>
            Старт курса: { date.transcriptionDate(content.start, 'd MMMM') }
          </div>
        </div>
        <div className={ styles.textContent }>
          <h3 className={ styles.title }>{ title }</h3>
          <p className={ styles.description }>{ description }</p>
        </div>
        <div className={ styles.listPrices }>
          <PriceBlock
            installment={ content.price.installment }
            price={ content.price.all }
            action={ content.action?.count }
          />
          <PriceBlock
            price={ content.price.all }
            action={ content.action?.count }
          />
          {
            content.action && (
              <div className={ styles.action }>
                <p className={ styles.title }>
                  Акция действует до { date.transcriptionDate(content.action.date, 'd MMMM') }
                </p>
                <h3 className={ styles.actionValue }>
                  -{ content.action.count }%
                </h3>
              </div>
            )
          }
        </div>
        <div className={ styles.promoSide }>
          <div className={ styles.promoInput }>
            <CustomInput props={{ type: 'text', value: promo, change: (value) => setPromo(value), placeholder: 'Промокод' }} />
          </div>
          <Button
            disabled={ !promo }
            text='Применить'
            color='grey'
            size='medium'
          />
        </div>
      </div>
      <div className={ styles.brief }>
        <Brief consultation />
      </div>
    </div>
  )
}