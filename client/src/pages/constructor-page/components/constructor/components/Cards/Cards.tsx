import {ICardsPageConstructor} from "@packages/api/typings/page-constructor.ts";
import styles from "./Cards.module.scss";

export const Cards = ({ content, title } : { content: ICardsPageConstructor[], title: string }) => {

  return (
    <div className={ styles.wrapper }>
      <h3 className={ styles.title }>
        { title }
      </h3>
      <div className={ styles.listCards }>
        {
          content.map(
            card => (
              <div className={ styles.cardBlock }>
                <p className={ styles.title }>{ card.title }</p>
                <p className={ styles.description }>{ card.description }</p>
              </div>
            )
          )
        }
      </div>
    </div>
  )
}