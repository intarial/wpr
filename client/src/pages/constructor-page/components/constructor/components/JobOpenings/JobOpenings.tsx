import styles from './JobOpenings.module.scss';
import {IJobOpeningsPageConstructor} from "@packages/api/typings/page-constructor.ts";

export const JobOpenings = ({ content, title } : { content: IJobOpeningsPageConstructor[], title: string }) => {

  return (
    <div className={ styles.wrapper }>
      <h3 className={ styles.title }>
        { title }
      </h3>
      <div className={ styles.listCards }>
        {
          content.map(
            card => (
              <div className={ styles.cardBlock }>
                <div className={ styles.leftSideContent }>
                  <p className={ styles.professionalName }>{ card.title }</p>
                  <p className={ styles.professionalDescription }>{ card.description }</p>
                </div>
                <p className={ styles.price }>
                  от { card.count?.toLocaleString('ru') } руб.
                </p>
              </div>
            )
          )
        }
      </div>
    </div>
  )
}