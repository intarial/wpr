import * as ChakraUI from "@chakra-ui/react";
import { PlayCircle } from "lucide-react";
import styles from "./List.module.scss";
import {IListPageConstructor} from "@packages/api/typings/page-constructor.ts";

export const List = ({ content, title, description }: { content: IListPageConstructor[], title: string, description?: string }) => {

  const { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel } = ChakraUI;

  return (
    <div className={styles.wrapper}>
      <div className={styles.list}>
        <div className={`${styles.listContent} width-1274px`}>
          <div className={styles.textContentWrapper}>
            <h3 className={styles.titleWrapper}>{title}</h3>
            <p className={styles.descriptionWrapper}>{description}</p>
          </div>
          <Accordion defaultIndex={[0]} allowMultiple className={styles.accordion}>
            {content.map(({ title, video, content }) => (
              <AccordionItem isDisabled={!content} className={styles.accordionItem} key={title}>
                {({ isDisabled, isExpanded }) => (
                  <>
                    <h2 className={styles.accordionItemHeader}>
                      <AccordionButton
                        className={`${styles.accordionButton} ${isDisabled && styles.accordionButtonDisabled}`}
                      >
                        <h4 className={styles.titleAccordion}>{title}</h4>
                        {!isDisabled && (
                          <div
                            className={`${styles.contentAccordionButton} ${isExpanded && styles.contentAccordionButtonActive}`}
                          >
                            {video && (
                              <div className={styles.accordionVideo}>
                                <PlayCircle />
                                Видеокурс
                              </div>
                            )}
                            <div className={styles.accordionItemIcon}>
                              <AccordionIcon />
                            </div>
                          </div>
                        )}
                      </AccordionButton>
                    </h2>
                    {video ? (
                      <AccordionPanel>
                        <div className={styles.video} />
                      </AccordionPanel>
                    ) : (
                      content?.length && (
                        <AccordionPanel>
                          <ul className={styles.listContentPanel}>
                            {content.map((value: string, index: number) => (
                              <li key={index}>{value}</li>
                            ))}
                          </ul>
                        </AccordionPanel>
                      )
                    )}
                  </>
                )}
              </AccordionItem>
            ))}
          </Accordion>
        </div>
      </div>
    </div>
  );
};