import {ISliderPageConstructor} from "@packages/api/typings/page-constructor.ts";
import {useState} from "react";
import styles from "./Slider.module.scss";
import {serverUrl} from "@constants/browser.ts";
import {SliderButtons} from "@ui/SliderButtons/SliderButtons.tsx";

export const Slider = ({ content, title } : { content: ISliderPageConstructor[], title: string }) => {
  const [activeSlide, setActiveSlide] = useState(0)

  const description = content[activeSlide].description.replace(/<SPACE>/g, "<br>");

  const updateActivePage = (type: 'plus' | 'minus'): void => {
    if (type === 'plus' && (activeSlide + 1) + 1 <= content.length) {
      setActiveSlide((prevActiveSlide) => prevActiveSlide + 1);
    }
    if (type === 'minus' && (activeSlide + 1) - 1 !== 0) {
      setActiveSlide((prevActivePage) => prevActivePage - 1);
    }
  };

  return (
    <div className={ styles.wrapperSlider }>
      <h3 className={ styles.titleWrapper }>
        { title }
      </h3>
      <div className={ styles.slider }>
        <div className={ styles.contentImage }>
          <img src={`${serverUrl}/images/profile/${ content[activeSlide].image }`} />
        </div>
        <div className={ styles.mainContent }>
          <div className={ styles.mainContentText }>
            <h5>{ content[activeSlide].title }</h5>
            <p
              dangerouslySetInnerHTML={{ __html: description }}
            />
          </div>
          <div className={ styles.sliderUpdater }>
            <SliderButtons
              activeValue={ activeSlide + 1 }
              countValue={ content.length }
              updateValue={ (type) => updateActivePage(type) }
              buttons={{
                color: 'grey',
                textNext: 'Дальше',
                textPrev: 'Назад',
                completeText: '',
                outText: ''
              }}
            />
          </div>
        </div>
      </div>
    </div>
  )
}