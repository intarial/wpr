import React, {useEffect, useState} from 'react';
import styles from './ConstructorPage.module.scss';
import {useNavigate, useParams} from "react-router-dom";
import api from "@packages/api";
import {Loader} from "@ui/Loader/Loader.tsx";
import {IPageConstructor} from "@packages/api/typings/page-constructor.ts";
import {ConstructorPageUtils} from "@utils/contructor-page/constructor-page.tsx";
import {ConstructorLayout} from "@components/layouts/constructor-layout/ConstructorLayout.tsx";

export const ConstructorPage: React.FC = () => {
    const { router } = useParams<{ router: string }>();
    const [pageContent, setPageContent] = useState<IPageConstructor[] | null>(null);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchData = async () => {
            try {
                if (router) {
                    const pageResponse = await api.pageApi.getPageByLabel(router);
                    const pageId = pageResponse.data.page._id;
                    const constructorResponse = await api.pageConstructorApi.getConstructorByPageId(pageId);
                    setPageContent(constructorResponse.data.constructor);
                }
            } catch (error) {
                console.log(error);
                navigate('profile')
            }
        };

        fetchData();
    }, [router]);

    if (!pageContent) return <Loader />;
    console.log(pageContent)

    return (
      <ConstructorLayout width1274={ false } className={ styles.wrapper }>
          {
              pageContent.map(
                content => (
                  <section className={[ styles.content, (content.type !== "list" && content.type !== 'reviews') && 'width-1274px' ].join(' ')}>
                      {
                          ConstructorPageUtils.getConstructorByLabel(content.type, content)
                      }
                  </section>
                )
              )
          }
      </ConstructorLayout>
    );
}