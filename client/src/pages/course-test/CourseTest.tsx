import styles from './CourseTest.module.scss';
import {MainLayout} from "@components/layouts/main-layout/MainLayout.tsx";
import InteractiveSlider from "@components/slider/interactive-slider/InteractiveSlider.tsx";
import {InteractiveConstants} from "@constants/interactive.ts";
import testApi from "@packages/api/src/test";
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {TestQAArrayTypes} from "@packages/api/typings/test.ts";
import { useNavigate } from 'react-router-dom';
import {addTestsUser} from "@packages/store/slices/user/user.ts";
import {useToast} from "@chakra-ui/react";
import userApi from "@packages/api/src/user";

export const CourseTest = () => {
  const user = useAppSelector((store) => store.user)
  const dispatch = useAppDispatch()
  const navigate = useNavigate();
  const toast = useToast()

  const testIsComplete = (test: TestQAArrayTypes) => {
    testApi.create({ user: user.user._id, testQAArray: test, type: 'career-guidance' })
      .then(
        res => {
          if (!res.data.test.answer || !res.data.test.answer?.content.course) {
            return toast({title: 'Ошибка', description: 'Повторите попытку', status: 'info'})
          }
          toast({ ...res.data.toast })

          dispatch(addTestsUser({ type: res.data.test.type, course: res.data.test.answer.content.course, _id: res.data.test._id }))
          navigate('profile')

          userApi.updateTests({ type: res.data.test.type, course: res.data.test.answer.content.course, _id: res.data.test._id })
        }
      )
  }

  return (
    <MainLayout fixedHeader footer={ false }>
      <div className={ styles.wrapper } >
        <InteractiveSlider
          closeTest={ () => navigate('profile') }
          interactive={{
            content: InteractiveConstants.testCourse,
            complete: (val) => testIsComplete(val)
          }}
        />
      </div>
    </MainLayout>
  )
}