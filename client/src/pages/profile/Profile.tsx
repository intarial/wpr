import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Loader } from 'lucide-react';
import { IUser } from '@packages/api/typings/user';
import api from '@packages/api';
import { ProfileUserSide } from '@components/profile/ProfileUserSide';
import { Button } from '@ui/Button/Button';
import { ProfileCourseBlock } from './components/profile-course-block/ProfileCourseBlock';
import { LearningPathways } from '@pages/landing/components/learning-pathways/LearningPathways';
import { CodeologyMaterials } from '@pages/landing/components/codeology-materials/CodeologyMaterials';
import styles from './Profile.module.scss';
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {MainLayout} from "@components/layouts/main-layout/MainLayout.tsx";
import {loginOutUser} from "@packages/store/slices/user/user.ts";

export const Profile = () => {
  const { user } = useAppSelector((store) => store);

  const { id } = useParams<{ id: string }>();
  const [userPage, setUserPage] = useState<IUser | null>(null);
  const dispatch = useAppDispatch()

  const itsMe = user.user._id === id

  const loginOut = () => {
    api.userApi.loginOut()
      .then(() => dispatch(loginOutUser()))
  }

  useEffect(() => {
    if (id) {
      api.userApi.getUserById(id).then(res => {
        setUserPage(res.data.user);
      });
    }
  }, [id]);

  if (!userPage) return <Loader />;

  return (
    <MainLayout className={ styles.wrapper }>
      <div className={ styles.profileContent }>
        <div className={styles.headerContent}>
          <ProfileUserSide user={userPage} />
          {itsMe && <Button onClick={ loginOut } size="small" text="Выйти из профиля"/>}
        </div>
        {itsMe && <ProfileCourseBlock/>}
      </div>
      {!userPage?.courses && <LearningPathways />}
      <CodeologyMaterials />
    </MainLayout>
  );
};

