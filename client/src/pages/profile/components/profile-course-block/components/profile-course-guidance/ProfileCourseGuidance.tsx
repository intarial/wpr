import styles from './ProfileCourseGuidance.module.scss';
import {Button} from "@ui/Button/Button.tsx";
import {TestTypes} from "@packages/api/typings/test.ts";
import {useAppSelector} from "@packages/store/hook.ts";
import {Spinner, Stack} from "@chakra-ui/react";

export const ProfileCourseGuidance = ({ test } : { test: { type: TestTypes, course: string, _id: string } }) => {
  const courses = useAppSelector((store) => store.courses);
  const course = courses.items.find(course => course._id === test.course);

  if (!course) {
    return (
      <Stack
        w={ '100%' }
        height={ '100%' }
        alignItems={ 'center' }
        justifyContent={ 'center' }
        direction='row'
        spacing={4}
      >
        <Spinner size='xl' color='blue.500' />
      </Stack>
    );
  }

  return (
    <div className={ styles.wrapper } >
      <div className={ styles.guidanceTextContent }>
        <p>Профессия</p>
        <h3>
          { course?.name }
        </h3>
      </div>
      <Button
        hover='orange'
        text='Подробнее'
        size='small'
        arrow={{ position: 'right', direction: 'right' }}
      />
    </div>
  )
}