import React from 'react';
import styles from './ProfileCoursesList.module.scss';
import {useAppSelector} from "@packages/store/hook.ts";

export const ProfileCoursesList: React.FC = () => {
  const user = useAppSelector((store) => store.user);


    return (
        <div className={ styles.wrapper } >
          {
            user.user.courses?.map(
              course => (
                <div>
                  { course }
                </div>
              )
            )
          }
        </div>
    )
}