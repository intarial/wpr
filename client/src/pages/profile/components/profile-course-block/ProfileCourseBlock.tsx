import React, {useEffect} from 'react';
import styles from './ProfileCourseBlock.module.scss';
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {IconArrowDropDown} from "@assets/icons";
import {
  ProfileCoursesList
} from "@pages/profile/components/profile-course-block/components/profile-courses-list/ProfileCoursesList.tsx";
import {setModalCourse} from "@packages/store/slices/modal/modal.ts";
import {CourseTestModal} from "@components/modals/course-test-modal/CourseTestModal.tsx";
import {
  ProfileCourseGuidance
} from "@pages/profile/components/profile-course-block/components/profile-course-guidance/ProfileCourseGuidance.tsx";

const DeactivatedCourseSide = () => {
  const dispatch = useAppDispatch()

  const openModal = () => {
    dispatch(setModalCourse({ type: 'test', value: true}))
  }

  return (
    <div onClick={ openModal } className={ styles.deactivatedCourseSide }>
      <div className={ styles.deactivatedCourseSideContent }>
        <p>Пройдите тест на профориентацию, и мы подберём вам подходящие курсы и профессии!</p>
        <IconArrowDropDown />
        <CourseTestModal />
      </div>
    </div>
  )
}

const TextCareerGuidance = () => {
  const dispatch = useAppDispatch()

  const openModal = () => {
    dispatch(setModalCourse({ type: 'test', value: true}))
  }

  return (
    <div className={styles.textCareerGuidance}>
      <h2>Ваш выбор - это</h2>
      <p>Данный ответ был подобран с использованием искусственного интелекта Open AI. Если вы не согласны с результатом,
        вы можете <span onClick={ () => openModal() }>снова пройти тест</span> для пересбора результата</p>
    </div>
  )
}

export const ProfileCourseBlock: React.FC = () => {
  const user = useAppSelector((store) => store.user);
  const careerGuidanceTest = user.user?.tests?.find(test => test.type && test.type === 'career-guidance');
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(setModalCourse({ type: 'test', value: false}))
  }, [])

  if (!user.user?.courses?.length) {
    if (careerGuidanceTest) {
      return (
        <div className={ styles.contentGuidance }>
          <TextCareerGuidance />
          <div className={styles.profileCourseBlock}>
            <ProfileCourseGuidance test={ careerGuidanceTest } />
          </div>
          <CourseTestModal />
        </div>
      )
    } else {
      return (
        <div className={styles.profileCourseBlock}>
          <DeactivatedCourseSide />
        </div>
      );
    }
  } else {
    return (
      <div className={styles.wrapper}>
        <ProfileCoursesList />
      </div>
    );
  }
};