import styles from './Validation.module.scss';
import {Header} from "@components/header/Header.tsx";
import {Footer} from "@components/footer/Footer.tsx";
import {Brief} from "@components/brief/Brief.tsx";
import {SwiperValidate} from "./components/SwiperValidate/SwiperValidate.tsx";
import {ValidateLayout} from "./components/ValidateLayout/ValidateLayout.tsx";

export const Validation = () => {
  return (
    <div className={ styles.wrapper } >
      <Header />
      <section className={[ styles.layout, 'width-1274px' ].join(' ')} >
        <main className={ styles.main }>
          <div className={ styles.validateContent }>
            <ValidateLayout />
            <SwiperValidate />
          </div>
          <Brief />
        </main>
      </section>
      <Footer />
    </div>
  )
}