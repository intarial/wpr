import React, {createContext} from 'react';
import styles from './ValidateLayout.module.scss';
import {useForm, UseFormSetValue} from "react-hook-form";
import {Login} from "../Login/Login.tsx";
import {Register} from "../Register/Register.tsx";
import {useLocation} from "react-router-dom";
import api from "@packages/api";
import {useToast} from "@chakra-ui/react";
import {authenticateUser} from "@packages/api/client/httpClient.ts";
import {useAppDispatch} from "@packages/store/hook.ts";
import {setUsersReduce} from "@packages/store/slices/user/user.ts";

interface ValidateForm {
    email: string
    password: string
    rePassword?: string
}

interface ValidateContext {
    value: ValidateForm
    update: UseFormSetValue<ValidateForm>
    send: () => void
}

export const ValidateContext = createContext<ValidateContext>({} as  ValidateContext)

export const ValidateLayout: React.FC = () => {
    const toast = useToast()
    const location = useLocation();
    const pathname = location.state.type;

    const dispatch = useAppDispatch()

    const { setValue, watch } = useForm<ValidateForm>({
        defaultValues: {
            email: "",
            password: "",
            rePassword: ''
        },
    });

    const handlerSend = async () => {
        if (pathname === 'register') {
            try {
                const res = await api.userApi.register({ email: watch('email'), password: watch('password') });
                res.data.toast && toast({ ...res.data.toast });
            } catch (err: any) {
                err.response.data.toast && toast({ ...err.response.data.toast });
            }
        } else {
            try {
                await api.userApi.login({ email: watch('email'), password: watch('password') })
                  .then(res => {
                      authenticateUser(res.data.token)
                        .then((res) => {
                            dispatch(setUsersReduce(res))
                        })
                      res.data.toast && toast({ ...res.data.toast });
                  });
            } catch (err: any) {
                err.response.data.toast && toast({ ...err.response.data.toast });
            }
        }
    };

    return (
      <div className={ styles.wrapper } >
          <ValidateContext.Provider value={{ value: { ...watch() }, update: setValue, send: handlerSend }}>
              {
                  pathname === 'login' ? (
                    <Login />
                  ) : pathname === 'register' && (
                    <Register />
                  )
              }
          </ValidateContext.Provider>
      </div>
    )
}