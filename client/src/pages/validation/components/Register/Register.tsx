import styles from './Register.module.scss';
import {ValidateContext} from "../ValidateLayout/ValidateLayout.tsx";
import {CustomInput} from "@ui/CustomInput/CustomInput.tsx";
import {Button} from "@ui/Button/Button.tsx";
import {NavLink} from "react-router-dom";
import {ROUTE_NAMES} from "@routes/const.ts";

export const Register = () => {
    return (
      <ValidateContext.Consumer>
          {
              validate => (
                <div className={ styles.wrapper } >
                    <h4>
                        Зарегестрируйте новый аккаунт
                    </h4>
                    <div className={ styles.content }>
                        <CustomInput
                          props={{
                              value: validate.value.email,
                              change: (val) => validate.update('email', val),
                              placeholder: 'Электронная почта'
                          }}
                        />
                        <CustomInput
                          props={{
                              value: validate.value.password,
                              change: (val) => validate.update('password', val),
                              placeholder: 'Пароль'
                          }}
                        />
                        <CustomInput
                          props={{
                              value: validate.value.password,
                              change: (val) => validate.update('rePassword', val),
                              placeholder: 'Повторите пароль'
                          }}
                        />
                        <div className={ styles.sendRequest }>
                            <Button
                              size={ 'medium' }
                              text={ 'Зарегестрироваться' }
                              color={ 'grey' }
                              onClick={ () => validate.send() }
                            />
                        </div>
                    </div>
                    <div className={ styles.switcher }>
                        <p className={ styles.textSwitch }>
                            Уже имеется профиль?
                        </p>
                        <NavLink
                          state={{ type: 'login' }}
                          to={ ROUTE_NAMES.VALIDATION }
                        >
                            <Button
                              size={ 'small' }
                              text={ 'Авторизоваться' }
                              color={ 'blue' }
                            />
                        </NavLink>
                    </div>
                </div>
              )
          }
      </ValidateContext.Consumer>
    )
}