import styles from './SwiperValidate.module.scss';
import {Swiper, SwiperSlide} from "swiper/react";

import "swiper/css";
import "swiper/css/effect-creative";
import "swiper/css/pagination";

import { EffectCreative, Autoplay, Pagination, Navigation } from "swiper";

export const SwiperValidate = () => {
  const items = [
    {
      name: 'Иван',
      image: 'https://i.imgur.com/95FiGHh.png',
      description: 'За полгода стал разработчиком и пришёл к стабильному доходу.'
    },
    {
      name: 'Андрей',
      image: 'https://i.imgur.com/t8jBRQj.png',
      description: 'Освоил навыки профессионального UI/UX дизайнера.'
    },
    {
      name: 'Иван',
      image: 'https://i.imgur.com/95FiGHh.png',
      description: 'За полгода стал разработчиком и пришёл к стабильному доходу.'
    },
  ]

  return (
    <div className={ styles.wrapper } >
      <Swiper
        autoplay={{
          delay: 5000,
          disableOnInteraction: false
        }}
        grabCursor={false}
        allowTouchMove={ false }
        navigation={true}
        effect={"creative"}
        creativeEffect={{
          prev: {
            shadow: true,
            translate: ["-20%", 0, -1],
          },
          next: {
            translate: ["100%", 0, 0],
          },
        }}
        pagination={{
          clickable: true,
        }}
        modules={[EffectCreative, Autoplay, Pagination, Navigation]}
        className={["mySwiper3", styles.swiper].join(' ')}
      >
        {
          items.map(
            (item, index) => (
              <SwiperSlide key={ index }>
                <div className={ styles.contenxBox }>
                  <img src={ item.image } />
                  <div className={ styles.filter }/>
                  <div className={ styles.nameContent }>
                    <h4 className={ styles.title }>
                      { item.name }
                    </h4>
                    <p className={ styles.description }>
                      { item.description }
                    </p>
                  </div>
                </div>
              </SwiperSlide>
            )
          )
        }
      </Swiper>
    </div>
  )
}