import styles from './Login.module.scss';
import {ValidateContext} from "../ValidateLayout/ValidateLayout.tsx";
import {CustomInput} from "@ui/CustomInput/CustomInput.tsx";
import {Button} from "@ui/Button/Button.tsx";
import {NavLink} from "react-router-dom";
import {ROUTE_NAMES} from "@routes/const.ts";
import {NotProfile} from "@ui/NotProfile/NotProfile.tsx";

export const Login = () => {
  return (
    <ValidateContext.Consumer>
      {
        validate => (
          <div className={ styles.wrapper } >
            <h4>
              Войдите в свой аккаунт
            </h4>
            <div className={ styles.content }>
              <CustomInput
                props={{
                  value: validate.value.email,
                  change: (val) => validate.update('email', val),
                  placeholder: 'Электронная почта'
                }}
              />
              <CustomInput
                props={{
                  value: validate.value.password,
                  change: (val) => validate.update('password', val),
                  placeholder: 'Пароль'
                }}
              />
              <Button
                size={ 'small' }
                text={ 'Я забыл свой пароль' }
                color={ 'grey' }
              />
              <div className={ styles.sendRequest }>
                <Button
                  onClick={ () => validate.send() }
                  size={ 'medium' }
                  text={ 'Войти' }
                  color={ 'grey' }
                />
              </div>
            </div>
            <NotProfile />
          </div>
        )
      }
    </ValidateContext.Consumer>
  )
}