import styles from './CodeologyMaterials.module.scss';
import {Button} from "@ui/Button/Button.tsx";
import {PlayCircle, Text} from "lucide-react";

interface IMaterialBlock {
  description: string
  type: 'video' | 'article'
}

const MaterialBlock = (instruction: IMaterialBlock) => (
  <div className={ styles.materialBlock }>
    <p className={ styles.materialBlockType }>
      {
        instruction.type === 'video' ? 'Видео' : 'Статья'
      }
    </p>
    <h5 className={ styles.materialBlockTitle }>
      { instruction.description }
    </h5>

    <div className={ styles.materialBlockButton }>
      {
        instruction.type === 'article' ? (
          <Button
            color={ 'grey' }
            size={ 'medium' }
            icon={ <Text /> }
            text={ 'Читать статью' }
          />
        ) : (
          <Button
            color={ 'grey' }
            size={ 'medium' }
            icon={ <PlayCircle /> }
            text={ 'Смотреть видео' }
          />
        )
      }
    </div>
  </div>
)
export const CodeologyMaterials = () => {
  const materials: IMaterialBlock[] = [
    { description: 'Старт карьеры: комьюнити и личный бренд с нуля', type: 'video' },
    { description: 'Что такое многозадачность и нужно ли её развивать', type: 'article' },
    { description: 'ChatGPT, RT-1, PaLM-E и другие нейросети, которые приближают восстание машин', type: 'article' },
    { description: 'IT в медицине: инфраструктура, ML, расшифровка визуальной информации, диагностика', type: 'article' },
  ]
  return (
    <section className={[ styles.layout, 'width-1274px' ].join(' ')} >
      <div className={ styles.wrapper } >
        <h4 className={ styles.title }>
          Материалы, что бы погрузится в IT
        </h4>
        <div className={ styles.listMaterials }>
          {
            materials.map(
              (material, index) => (
                <MaterialBlock
                  key={ index }
                  type={ material.type }
                  description={ material.description }
                />
              )
            )
          }
        </div>
      </div>
    </section>
  )
}