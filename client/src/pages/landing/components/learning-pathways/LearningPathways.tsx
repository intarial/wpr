import React from 'react';
import styles from './LearningPathways.module.scss';
import 'swiper/css';
import './swiper.css'
import {useAppSelector} from "@packages/store/hook.ts";
import {ICourseItem} from "@packages/api/typings/courses.ts";
import {dateUtils} from "@utils/date";
import {Button} from "@ui/Button/Button.tsx";
import {CustomSwiper} from "@ui/CustomSwiper/CustomSwiper.tsx";
import {DeclensionsUtils} from "@utils/declensionsUtils.ts";
import {CourseSkeleton} from "@components/skeletons/CourseSkeleton/CourseSkeleton.tsx";

const PathwayBlock: React.FC<{ item: ICourseItem, isLoaded: boolean }> = ({ item, isLoaded }) => {
  return (
    <CourseSkeleton.Link
      loaded={ isLoaded }
    >
      <div className={ styles.pathwayBlock }>
        <div className={ styles.titleContent }>
          <h6>
            Профессия
          </h6>
          <h4 className={ styles.title }>
            { item.name }
          </h4>
          <p className={ styles.term }>
            { dateUtils.date.weeksLengthToMonth(item.term) } { DeclensionsUtils.month(dateUtils.date.weeksLengthToMonth(item.term)) }
          </p>
        </div>
        <Button
          color={ 'grey' }
          size={ 'small' }
          arrow={{ position: 'right', direction: 'right' }}
        />
      </div>
    </CourseSkeleton.Link>
  )
}

export const LearningPathways: React.FC = () => {
  const {
    courses: {
      items
    }
  } = useAppSelector((store) => store);

  const isLoaded = items.length > 0
  const renderItems = isLoaded ? items : new Array(10).fill(0)

  return (
    <section className={ styles.wrapper } >
      <div className={[ styles.listPathways, 'width-1274px' ].join(' ')} >
        <h3 className={ styles.title }>
          Доступные направления
        </h3>
        <div>
          <CustomSwiper
            items={ renderItems }
            render={(item: ICourseItem) => (
              <PathwayBlock
                isLoaded={ isLoaded }
                item={ item }
              />
            )}
          />
        </div>
      </div>
    </section>
  )
}