import styles from './AdvantageSection.module.scss';

import {
  Flame,
  SmilePlus,
  Star,
  ThumbsUp
} from "lucide-react";

type AdvantageBlockType = {
  text: string
  icon?: JSX.Element
}

const AdvantageBlock = ({ text, icon }: AdvantageBlockType) => {
  return (
    <div className={ styles.advantageBlock }>
      { icon }
      <p>
        { text }
      </p>
    </div>
  )
}

export const AdvantageSection = () => {
  const advantages: AdvantageBlockType[] = [
    {
      text: 'Изучаете материал на платформе в любое удобное время',
      icon: <Flame />
    },
    {
      text: 'Изучаете материал на платформе в любое удобное время',
      icon: <Star />
    },
    {
      text: 'Изучаете материал на платформе в любое удобное время',
      icon: <ThumbsUp />
    },
    {
      text: 'Изучаете материал на платформе в любое удобное время',
      icon: <SmilePlus />
    },
  ]

  return (
    <section className={[ styles.wrapper, 'width-1274px' ].join(' ')} >
      {
        advantages.map(
          (advantage, index) => <AdvantageBlock key={ index } icon={ advantage.icon } text={  advantage.text } />
        )
      }
    </section>
  )
}