import React from 'react';
import styles from './Instruction.module.scss';

interface IInstructionBlock {
    title: string
    description: string
}

const InstructionBlock = (instruction: IInstructionBlock) => (
  <div className={ styles.instructionBlock }>
      <h5 className={ styles.instructionBlockTitle }>
          { instruction.title }
      </h5>
      <p className={ styles.instructionBlockDescription }>
          { instruction.description }
      </p>
  </div>
)

export const Instruction: React.FC = () => {
    const instructions: IInstructionBlock[] = [
        { title: 'Личный кабинет студента', description: 'В нем ты будешь смотреть лекции, прямые трансляции и дополнительные материалы, а ещё отправлять домашки на проверку нашим экспертам.' },
        { title: 'Обучение в комфортном темпе', description: 'Все лекции откроются сразу или по мере прохождения курса. Когда посмотреть — решаешь сам.' },
        { title: 'Доступ навсегда', description: 'Даже к обновлённому контенту. Сможешь вернуться к пройденному материалу в любой момент.' },
        { title: 'Много практики', description: 'Например, домашние задания, а ещё учебные и дипломные проекты. Поддерживать и направлять тебя будут действующие специалисты.' },
    ]

    return (
      <section className={[ styles.layout, 'width-1274px' ].join(' ')} >
          <div className={ styles.wrapper } >
              <h4 className={ styles.title }>
                  Обучение в Кодологии - это
              </h4>
              <div className={ styles.listInstruction }>
                  {
                      instructions.map(
                        instruction => (
                          <InstructionBlock
                            key={ instruction.title }
                            title={ instruction.title }
                            description={ instruction.description }
                          />
                        )
                      )
                  }
              </div>
          </div>
      </section>
    )
}