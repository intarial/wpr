import styles from './CodeologyTeachers.module.scss';

interface ITeacher {
    image: string
    name: string
    profile: string
}

const teachers: ITeacher[] = [
    { image: 'https://i.imgur.com/Vkq76lv.png', name: 'Иван иванов', profile: 'Руководитель разработки в Яндексе' },
    { image: 'https://i.imgur.com/VmH7q08.png', name: 'Кристина Иванова', profile: 'Product Analyst Team Lead в Ozon' },
    { image: 'https://i.imgur.com/emfgkIA.png', name: 'Игорь Иванов', profile: 'R&D-разработчик в Lamoda' },
    { image: 'https://i.imgur.com/R2xrb6o.png', name: 'Владимир Иванов', profile: 'Старший разработчик в Яндекс Дзене' },
]

const TeacherBlock = ({ teacher }: { teacher: ITeacher }) => (
    <div style={{ backgroundImage: `url(${teacher.image})` }} className={ styles.teacherBlock }>
        <div className={ styles.teacherBlockInfo }>
            <p className={ styles.name }>
                { teacher.name }
            </p>
            <p className={ styles.profile }>
                { teacher.profile }
            </p>
        </div>
    </div>
)

export const CodeologyTeachers = () => {

    return (
      <section className={[ styles.layout, 'width-1274px' ].join(' ')} >
          <div className={ styles.wrapper } >
              <h4 className={ styles.title }>
                  Наши ведущие преподаватели
              </h4>
              <div className={ styles.listTeachers }>
                  {
                      teachers.map(
                        teacher => <TeacherBlock key={ teacher.image } teacher={ teacher } />
                      )
                  }
              </div>
          </div>
      </section>
    )
}