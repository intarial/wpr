import React from 'react';
import styles from './CareerAdvancement.module.scss';
import {Button} from "@ui/Button/Button";
import imageCareer from '@assets/images/landing/career-advancement.png'

export const CareerAdvancement: React.FC = () => {
  return (
    <section className={[ styles.layout, 'width-1274px' ].join(' ')} >
      <div className={styles.wrapper} >
        <div className={ styles.textContent }>
          <p className={ styles.description }>
            Вместе с Кодологией
          </p>
          <h2 className={ styles.title }>
            Станьте востребованным специалистом
          </h2>
        </div>
        <Button
          style={{
            //@ts-ignore
            marginTop: 'auto'
          }}
          text={ 'Смотреть все курсы' }
          size={ 'small' }
          arrow={{
            position: 'right',
            direction: 'right'
          }}
        />
        <img className={ styles.imageCareer } src={ imageCareer } />
      </div>
    </section>
  )
}