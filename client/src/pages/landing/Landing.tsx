import styles from './Landing.module.scss';
import {Header} from "@components/header/Header";
import {CareerAdvancement} from "./components/career-advancement/CareerAdvancement";
import {LearningPathways} from "./components/learning-pathways/LearningPathways";
import {AdvantageSection} from "./components/advantage-section/AdvantageSection.tsx";
import {Offer} from "@components/offer/Offer.tsx";
import {useAppSelector} from "@packages/store/hook.ts";
import {Instruction} from ".//components/instruction/Instruction.tsx";
import {Reviews} from "@components/reviews/Reviews.tsx";
import {Brief} from "@components/brief/Brief.tsx";
import {CodeologyTeachers} from "./components/codeology-teachers/CodeologyTeachers.tsx";
import {CodeologyMaterials} from "./components/codeology-materials/CodeologyMaterials.tsx";
import {Footer} from "@components/footer/Footer.tsx";

export const Landing = () => {
  const {
    offers: {
      landing
    }
  } = useAppSelector((store) => store);

  return (
    <div className={[ styles.wrapper ].join(' ')} >
      <Header />
      <div className={ styles.content }>
        <CareerAdvancement />
        <LearningPathways />
        <AdvantageSection />
        {
          landing && (
            <Offer
              offer={ landing }
            />
          )
        }
        <Instruction />
        <Reviews />
        <Brief />
        <CodeologyTeachers />
        <CodeologyMaterials />
        <Footer />
      </div>
    </div>
  )
}