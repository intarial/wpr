import { configureStore } from '@reduxjs/toolkit'
import coursesReducer from './slices/courses.ts';
import offersReducer from './slices/offers.ts';
import reviewReducer from './slices/review.ts';
import userReducer from './slices/user/user.ts';
import modalReducer from './slices/modal/modal.ts';

const store = configureStore({
    reducer: {
        courses: coursesReducer,
        offers: offersReducer,
        review: reviewReducer,
        user: userReducer,
        modal: modalReducer
    }
})

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
