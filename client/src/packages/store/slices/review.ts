import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {IReview} from "@packages/api/typings/review.ts";

type StoreCoursesState = {
  items: IReview[]
}

const initialState: StoreCoursesState = {
  items: [
    {
      _id: 'fg54gvdfvro3495',
      user_id: '6464ef5b70c8626dfa288410',
      portfolio: 'fds',
      text: 'Я программист. Долгое время занималась разработкой и внедрением систем, а затем руководила IT-подразделениями крупных энергетических компаний. Сейчас для меня настало время перемен, и я подумала, что нужно обновить знания. Тем более что теоретических основ по управлению проектам'
    },
    {
      _id: 'fg54gvdfvro3495',
      user_id: '6464ef5b70c8626dfa288410',
      text: 'Я программист. Долгое время занималась разработкой и внедрением систем, а затем руководила IT-подразделениями крупных энергетических компаний. Сейчас для меня настало время перемен, и я подумала, что нужно обновить знания. Тем более что теоретических основ по управлению проектам'
    },
    {
      _id: 'fg54gvdfvro3495',
      user_id: '6464ef5b70c8626dfa288410',
      text: 'Я программист. Долгое время занималась разработкой и внедрением систем, а затем руководила IT-подразделениями крупных энергетических компаний. Сейчас для меня настало время перемен, и я подумала, что нужно обновить знания. Тем более что теоретических основ по управлению проектам'
    },
    {
      _id: 'fg54gvdfvro3495',
      user_id: '6464ef5b70c8626dfa288410',
      video: 'fds',
      text: 'Я программист. Долгое время занималась разработкой и внедрением систем, а затем руководила IT-подразделениями крупных энергетических компаний. Сейчас для меня настало время перемен, и я подумала, что нужно обновить знания. Тем более что теоретических основ по управлению проектам'
    },
    {
      _id: 'fg54gvdfvro3495',
      user_id: '6464ef5b70c8626dfa288410',
      text: 'Я программист. Долгое время занималась разработкой и внедрением систем, а затем руководила IT-подразделениями крупных энергетических компаний. Сейчас для меня настало время перемен, и я подумала, что нужно обновить знания. Тем более что теоретических основ по управлению проектам'
    },
    {
      _id: 'fg54gvdfvro3495',
      user_id: '6464ef5b70c8626dfa288410',
      video: 'fds',
      portfolio: 'fds',
      text: 'Я программист. Долгое время занималась разработкой и внедрением систем, а затем руководила IT-подразделениями крупных энергетических компаний. Сейчас для меня настало время перемен, и я подумала, что нужно обновить знания. Тем более что теоретических основ по управлению проектам'
    },
  ]
}

const reviewSlice = createSlice({
  name: 'review',
  initialState,

  reducers: {
    setReviewsReduce(state, action: PayloadAction<IReview[]>) {
      state.items = action.payload;
    }
  }
});

export const {
  setReviewsReduce
} = reviewSlice.actions;
export default reviewSlice.reducer;