import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {IOfferItem} from "@packages/api/typings/offers.ts";

type StoreCoursesState = {
  items: IOfferItem[],
  landing?: IOfferItem
}

const initialState: StoreCoursesState = {
  items: [
    {
      _id: 'fds32r32f5g4',
      title: 'Весенняя распродажа - скидки до 60%',
      date_start: new Date().toString(),
      date_end: (new Date(new Date().toTimeString() + 20000)).toString()
    }
  ],
  landing: {
    _id: 'fds32r32f5g4',
    title: 'Весенняя распродажа - скидки до 60%',
    date_start: new Date().toString(),
    date_end: '2023-05-02'
  }
}

const offersSlice = createSlice({
  name: 'offers',
  initialState,

  reducers: {
    setOffersReduce(state, action: PayloadAction<IOfferItem[]>) {
      state.items = action.payload;
    },
    setLandingOfferReduce(state, action: PayloadAction<string>) {
      state.landing = state.items.filter(({ _id }) => _id === action.payload)[0];
    }
  }
});

export const {
  setOffersReduce,
  setLandingOfferReduce
} = offersSlice.actions;
export default offersSlice.reducer;