import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type CourseModalsType = 'test'

type IImageModal = {
  url: string
  size: '16:10' | '3:5'
}

type StoreCoursesState = {
  image?: string | IImageModal
  course: {
    test: boolean
  }
}

const initialState: StoreCoursesState = {
  course: {
    test: false
  }
}

const modalSlice = createSlice({
  name: 'modal',
  initialState,

  reducers: {
    setModalImage(state, action: PayloadAction<string | undefined | IImageModal>) {
      state.image = action.payload;
    },
    setModalCourse(state, action: PayloadAction<{ type: CourseModalsType, value: boolean }>) {
      state.course[action.payload.type] = action.payload.value;
    }
  }
});

export const {
  setModalImage,
  setModalCourse
} = modalSlice.actions;
export default modalSlice.reducer;