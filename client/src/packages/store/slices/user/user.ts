import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {IUser} from "@packages/api/typings/user.ts";
import {TestTypes} from "@packages/api/typings/test.ts";

type StoreCoursesState = {
  user: IUser
  isLoginIn: boolean
}

const initialState: StoreCoursesState = {
  user: {} as IUser,
  isLoginIn: false
}

const userSlice = createSlice({
  name: 'user',
  initialState,

  reducers: {
    setUsersReduce(state, action: PayloadAction<{ user: IUser }>) {
      state.user = action.payload.user;
      state.isLoginIn = true
    },
    addTestsUser(state, action: PayloadAction<{ type: TestTypes, course: string, _id: string }>) {
      if (state.user.tests.find(test => test._id === action.payload._id)) {
        state.user.tests.map(test => {
          if (test._id === action.payload._id) {
            return action.payload;
          }
          return test;
        });
      } else {
        state.user.tests = [...state.user.tests, action.payload];
      }
    },
    updateUserAvatar(state, action: PayloadAction<string>) {
      state.user.avatar = action.payload;
    },
    loginOutUser(state) {
      state.isLoginIn = false;
    }
  }
});

export const {
  setUsersReduce,
  updateUserAvatar,
  addTestsUser,
  loginOutUser
} = userSlice.actions;
export default userSlice.reducer;