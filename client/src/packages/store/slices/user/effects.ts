import { createEffect } from "effector";
import api from "@packages/api";

export const getProfileFx = createEffect(async () => {
  const me = await api.userApi.getMe();
  const { data } = await api.userApi.getProfile(me.data.user_id);

  return data;
});
