import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ICourseItem } from "@packages/api/typings/courses.ts";

type StoreCoursesState = {
  items: ICourseItem[]
}

const initialState: StoreCoursesState = {
  items: []
}

const coursesSlice = createSlice({
  name: 'courses',
  initialState,

  reducers: {
    setCoursesReduce(state, action: PayloadAction<ICourseItem[]>) {
      state.items = action.payload;
    },
  }
});

export const {
  setCoursesReduce
} = coursesSlice.actions;
export default coursesSlice.reducer;