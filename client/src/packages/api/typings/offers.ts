export interface IOfferItem {
  _id: string
  title: string
  date_start: string
  date_end: string
}