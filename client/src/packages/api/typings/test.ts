export const TestTypesEnum = {
  CareerGuidance: 'career-guidance',
} as const;

export type TestTypes = typeof TestTypesEnum[keyof typeof TestTypesEnum];
export type TestQAArrayTypes = { question: string; answer: string; }[]
export type CareerGuidanceTypes = 'react-developer' | 'designer' | 'backend-developer'

export type TestAnswerTypes = {
  name: string
  content: {
    label?: string,
    value?: string,
    course?: string
    error?: string
  }
}

export interface ICreateAnswer {
  user: string;
  type: TestTypes;
  testQAArray: TestQAArrayTypes
}

export interface ITestTypes {
  _id: string
  user: string;
  type: TestTypes;
  testQAArray: TestQAArrayTypes
  answer?: TestAnswerTypes;
}

export interface ITestTypesResponse {
  user: string;
  type: TestTypes;
  testQAArray: TestQAArrayTypes
  answer?: TestAnswerTypes;
}