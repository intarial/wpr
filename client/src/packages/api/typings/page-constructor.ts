import {TPageConstructorsTypes} from "@packages/api/typings/page.ts";

export interface ISliderPageConstructor {
  image: string
  title: string
  description: string
}

export interface IListPageConstructor {
  title: string
  content: string[]
  video?: string
}

export interface ICardsPageConstructor {
  title: string
  description: string
  image?: string
}

export interface IJobOpeningsPageConstructor {
  title: string
  description: string
  count: number
}

export interface IPricesPageConstructor {
  title: string
  description: string
  price: {
    all: number
    installment?: number
  }
  action?: {
    date: string
    count: number
  }
  promos?: string[]
  start: string
}

export interface IPageConstructor<T extends TPageConstructorsTypes = TPageConstructorsTypes> extends Document {
  _id: string
  type: T;
  title: string
  description?: string
  values: T extends 'slider' ? ISliderPageConstructor[] : T extends 'list' ? IListPageConstructor[]: T extends 'prices' ? IPricesPageConstructor[]: T extends 'job_openings' ? IJobOpeningsPageConstructor[] : never;
}