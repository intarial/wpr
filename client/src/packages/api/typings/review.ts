export interface IReview {
  _id: string
  text: string
  user_id: string
  video?: string
  portfolio?: string
}