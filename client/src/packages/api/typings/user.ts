import {TestTypes} from "@packages/api/typings/test.ts";

export interface IValidation {
  email: string
  password: string
}

export interface IUser {
  _id: string
  firstName?: string
  lastName?: string
  email: string
  birthday?: string
  address?: string
  phone?: string
  avatar?: string
  courses?: string[] // ID направления в разработке
  tests: { type: TestTypes, course: string, _id: string }[]
  password: string,
  isActive: boolean,
  activeCode: number,
}

export interface UserUpdateProps {
  _id: string
  firstName?: string
  lastName?: string
  email?: string
  birthday?: string
  address?: string
  phone?: string
  avatar?: string
}

export interface UserPasswordUpdateProps {
  _id: string
  password: string
  newPassword: string
  newRePassword: string
}

export interface IBrief {
  name: string
  phone: string
  email: string
}