export interface AuthTokens {
  access: string;
}

export interface GetMeResponse {
  user_id: string;
}