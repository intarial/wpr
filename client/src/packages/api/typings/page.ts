export type TPageConstructorsTypes = 'slider' | 'swiper' | 'promotion' | 'list' | 'prices' | 'cards' | 'pathways' | 'reviews' | 'materials' | 'job_openings'

export interface IPage extends Document {
  _id: string
  title: string
  router: string
  values: string[]
}