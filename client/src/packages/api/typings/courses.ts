import {IUser} from "@packages/api/typings/user.ts";
import {TestTypes} from "@packages/api/typings/test.ts";

export interface ICourseItem {
  _id: string
  name: string
  label: TestTypes
  term: number
  students: IUser[]
}