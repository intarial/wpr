import axios from 'axios';
import {ACCESS_TOKEN} from "@constants/cookies.ts";
import Cookies from "js-cookie";
import {getProfileFx} from "@packages/store/slices/user/effects.ts";

export const httpClient = axios.create({
    baseURL: 'http://localhost:3333/api/',
    timeout: 10000,
    headers: {}
});

httpClient.interceptors.request.use(
  async (response) => {
      if (!response.headers.get("Authorization") && Cookies.get(ACCESS_TOKEN)) {
          response.headers.Authorization = `JWT ${Cookies.get(ACCESS_TOKEN)}`;
      }

      return response;
  },
  (error) => {
      return Promise.reject(error);
  }
);

export const authenticateUser = (token: string) => {
    Cookies.set(ACCESS_TOKEN, token);
    httpClient.defaults.headers.Authorization = `JWT ${ token }`;
    return getProfileFx();
};
