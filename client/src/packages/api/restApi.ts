import courseApi from './src/course';
import {userApi} from "@packages/api/src/user/base.ts";
import {pageApi} from "@packages/api/src/page/base.ts";
import {pageConstructorApi} from "./src/page-constructor/base.ts";

export const restApi = {
    userApi,
    pageApi,
    pageConstructorApi,
    courseApi
};
