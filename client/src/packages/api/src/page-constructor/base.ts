import client from "../../client";
import {IPageConstructor} from "@packages/api/typings/page-constructor.ts";

const baseApiUrlTest = 'page-constructor'

export const pageConstructorApi = {
  getConstructorByPageId: async (page_id: string) => {
    return client.get<{ constructor: IPageConstructor[] }>(`${baseApiUrlTest}/get-in-page`, { params: { page_id } });
  },
};
