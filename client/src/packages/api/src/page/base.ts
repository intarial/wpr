import client from "../../client";
import {IPage} from "@packages/api/typings/page.ts";

const baseApiUrlTest = 'page'

export const pageApi = {
  getPageByLabel: async (router: string) => {
    return client.get<{ page: IPage }>(`${baseApiUrlTest}/get`, { params: { router } });
  },
};
