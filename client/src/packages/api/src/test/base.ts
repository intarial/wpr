import client from "../../client";
import {ITestTypes, ITestTypesResponse} from "@packages/api/typings/test.ts";
import {IToastResponse} from "@ui/ui.types.ts";

const baseApiUrlTest = 'tests'

export const testApi = {
  create: async (body: ITestTypesResponse) => {
    return await client.post<{ test: ITestTypes, toast: IToastResponse }>(`${baseApiUrlTest}/create/`, {
      ...body,
    })
  },
};
