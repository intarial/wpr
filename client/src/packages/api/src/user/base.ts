import client from "../../client";
import {IUser, IValidation, UserUpdateProps} from "../../typings/user.ts";
import {IToast} from "@ui/ui.types.ts";
import {extractToken} from "@packages/api/utils/extractToken.ts";
import {GetMeResponse} from "@packages/api/typings/auth.ts";
import Cookies from "js-cookie";
import {ACCESS_TOKEN} from "@constants/cookies.ts";
import {TestTypes} from "@packages/api/typings/test.ts";

const baseApiUrlValidate = "validate";
const baseApiUrlUser = "user";

type RegisterTypeResponse = { toast: IToast, token: string }

export const userApi = {
  register: async (body: IValidation) => {
    return await client.post<RegisterTypeResponse>(`${baseApiUrlValidate}/register/`, {
      ...body,
    })
  },

  login: async (body: IValidation) => {
    return await client.post<RegisterTypeResponse>(`${baseApiUrlValidate}/login/`, {
      ...body,
    })
  },

  getMe: async () => {
    const params = { token: extractToken(client) };
    return client.post<GetMeResponse>(`${baseApiUrlValidate}/me/`, params);
  },

  getProfile: async (user_id: string) => {
    return client.get<IUser>(`${baseApiUrlUser}/${user_id}/`);
  },

  getUserById: async (id: string) => {
    return await client.get<{ user: IUser }>(`${baseApiUrlUser}/${ id }`);
  },

  getAllUsers: async () => {
    return await client.get(`${baseApiUrlUser}/all`);
  },

  updateProfile: async (body: UserUpdateProps) => {
    const user_id = body._id
    return client.put<{ user: IUser }>(`${baseApiUrlUser}/update/profile/${user_id}`, { ...body });
  },

  updatePassword: async (body: { [p: string]: string; _id: any }) => {
    const user_id = body._id
    return client.put<{ user: IUser }>(`${baseApiUrlUser}/update/password/${user_id}`, { ...body });
  },

  loginOut: async () => {
    Cookies.remove(ACCESS_TOKEN)
  },

  uploadProfileImage: async (file: FileList ) => {

    const formData = new FormData();
    formData.append('image', file[0]);

    return client.put<{ avatar: string }>(`${baseApiUrlUser}/update/image`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  },

  updateTests: async ({ type, course, _id } : { type: TestTypes, course: string, _id: string }) => {
    return client.put<{ avatar: string }>(`${baseApiUrlUser}/update/image`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  }
};
