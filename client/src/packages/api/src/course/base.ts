import client from "../../client";

const baseApiUrlCourse = "http://localhost:3333/api/course";

export const courseApi = {
  getCourseById: async (id: string) => {
    return {
      data: {
        result: {
          _id: '32g0vt49342',
          name: 'Python разработчик',
          term: 16
        }
      }
    }

    return client.get(`${baseApiUrlCourse}/by-id`, {
      params: {
        id
      }
    })
  },
  getAllCourses: async () => {
    return client.get(`${baseApiUrlCourse}/all`)
  }
};
