import { AxiosInstance } from "axios";

export const extractToken = (http: AxiosInstance) => {
  const header = http.defaults.headers.Authorization as string;
  const [, token] = header.split(" ");
  return token;
};
