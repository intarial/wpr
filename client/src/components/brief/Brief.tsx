import styles from './Brief.module.scss';
import {CustomInput} from "@ui/CustomInput/CustomInput.tsx";
import {Button} from "@ui/Button/Button.tsx";
import {Link} from "react-router-dom";
import {NotProfile} from "@ui/NotProfile/NotProfile.tsx";

export const Brief = ({ consultation = false } : { consultation?: boolean }) => {

  if (consultation) {
    return  (
      <div className={ styles.consultation }>
        <p className={ styles.title }>
          Запишитесь или получите консультацию
        </p>
        <div className={ styles.inputsTemplate }>
          <CustomInput props={{ placeholder: 'Имя' }} />
          <CustomInput props={{ placeholder: 'Телефон' }} />
          <CustomInput props={{ placeholder: 'Электронная почта' }} />
        </div>
        <p className={ styles.info }>
          Нажимая на кнопку, я соглашаюсь на обработку <Link to={ '' }>персональных данных</Link>
        </p>
        <div className={ styles.buttons }>
          <Button text='Записаться' color='blue' size='medium' />
          <Button color='grey' text='Получить консультацию' size='medium' />
        </div>
        <NotProfile />
      </div>
    )
  }

  return (
    <section className={[ styles.layout, 'width-1274px' ].join(' ')} >
      <div className={ styles.wrapper } >
        <div className={ styles.textContent }>
          <p className={ styles.preTitle }>
            Мы всегда
          </p>
          <h3 className={ styles.title }>
            Поможем в выборе направления бесплатно
          </h3>
          <p className={ styles.description }>
            Если у вас есть вопросы о формате или вы не знаете, что выбрать, оставьте свой номер — мы позвоним, чтобы ответить на все ваши вопросы.
          </p>
        </div>
        <div className={ styles.briefContent }>
          <div className={ styles.gridTemplate }>
            <CustomInput props={{ placeholder: 'Имя' }} />
            <CustomInput props={{ placeholder: 'Телефон' }} />
            <CustomInput props={{ placeholder: 'Электронная почта' }} />
          </div>
          <div className={ styles.sendData }>
            <Button
              text={ 'Отправить' }
              color={ 'blue' }
              size={ 'medium' }
            />
            <p className={ styles.info }>
              Нажимая на кнопку, я соглашаюсь на обработку <Link to={ '' }>персональных данных</Link> и с <Link to={''}>правилами пользования Платформой</Link>
            </p>
          </div>
        </div>
      </div>
    </section>
  )
}