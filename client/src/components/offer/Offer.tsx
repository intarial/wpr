import styles from './Offer.module.scss';
import { Button } from '@ui/Button/Button.tsx';
import { IconOfferPattern } from '@assets/icons';
import { time } from '@utils/date/time.ts';
import { useEffect, useState } from 'react';
import { IOfferItem } from '@packages/api/typings/offers.ts';

interface Props {
  offer: IOfferItem;
}

export const Offer = ({ offer }: Props) => {
  const [timer, setTimer] = useState(time.getTimerInDate(offer.date_end));

  useEffect(() => {
    const intervalId = setInterval(() => {
      setTimer(time.getTimerInDate(offer.date_end));
    }, 1000);

    return () => clearInterval(intervalId);
  }, [offer.date_end]);

  if (!offer || !timer) {
    return null;
  }

  return (
    <section className={[styles.layout, 'width-1274px'].join(' ')}>
      <div className={styles.wrapper}>
        <div className={styles.offerPattern}>
          <IconOfferPattern />
        </div>
        <div className={styles.content}>
          <div className={styles.description}>
            <p className={styles.preTitle}>Не упусти шанс начать обучение выгодно</p>
            <h3 className={styles.title}>{ offer.title }</h3>
            <Button
              text="Подробнее"
              size="small"
              hover="orange"
              arrow={{
                position: 'right',
                direction: 'right',
              }}
              style={{
                //@ts-ignore
                marginTop: 'auto',
              }}
            />
          </div>
          <div>
            <div className={styles.timer}>{timer}</div>
          </div>
        </div>
      </div>
    </section>
  );
};