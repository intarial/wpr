import styles from './ImageModal.module.scss';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
} from '@chakra-ui/react'
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {setModalImage} from "@packages/store/slices/modal/modal.ts";

export const ImageModalProvider = () => {
  const { modal: { image } } = useAppSelector((store) => store);
  const dispatch = useAppDispatch()

  const closeModal = () => dispatch(setModalImage(undefined))

  const imageReturn = () => {
    if (typeof image === "string") {
      return image
    } else {
      return image?.url
    }
  }

  return (
    <>
      <Modal isCentered isOpen={ !!image } onClose={closeModal}>
        <ModalOverlay />
        <ModalContent
          className={ styles.modal }
        >
          <ModalBody>
            <img
              onClick={ closeModal }
              src={ imageReturn() }
            />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}