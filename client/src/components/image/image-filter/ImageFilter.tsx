import styles from './ImageFilter.module.scss';
import {Camera} from "lucide-react";
import {CustomAvatar} from "@ui/CustomAvatar/CustomAvatar.tsx";
import {useAppDispatch} from "@packages/store/hook.ts";
import {setModalImage} from "@packages/store/slices/modal/modal.ts";
import {serverUrl} from "@constants/browser.ts";
import {useRef} from "react";
import {UploadFile} from "@utils/upload-file";
import {updateUserAvatar} from "@packages/store/slices/user/user.ts";

interface IImageFilter {
  circle: boolean
  size: 'x50' | 'x120'
  change?: boolean
  url?: string
  name?: string
  borderRadius?: string
}

const Filter = () => {
  return (
    <div className={ styles.filter }>
      <Camera />
    </div>
  )
}

export const ImageFilter = (info: IImageFilter) => {

  const classNames = [styles.wrapper]
  info.size && classNames.push(styles[info.size])
  info.change && classNames.push(styles.change)
  info.url && classNames.push(styles.pointer)

  const dispatch = useAppDispatch()

  const openFullSizeImage = () => {
    if (!info.url) return

    dispatch(setModalImage(`${serverUrl}/images/profile/${ info.url }`))
  }

  const inputRef = useRef<HTMLInputElement>(null);

  const onInputClick = () => {
    if (inputRef.current) {
      inputRef.current.click();
    }
  };

  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log('fd12s')
    const file = event.target.files;
    console.log('fd1s')
    if (!file) return

    console.log('fds')

    UploadFile.profile(file)
      .then(
        res => {
          console.log(res)
          dispatch(updateUserAvatar(res.data.avatar))
        }
      )
  };

  const imageClick = () => {
    if (!info.change && info.url) {
      openFullSizeImage();
    } else {
      onInputClick();
    }
  };

  return (
    <div
      style={{
        borderRadius: info.circle ? '50%' : info.borderRadius ?? 0
      }}
      className={ classNames.join(' ') }
      onClick={ imageClick }
    >
      {
        !info.url ? (
          <CustomAvatar
            width='x120'
            name={ info.name }
          />
        ) : (
          <img src={ `${serverUrl}/images/profile/${ info.url }` } />
        )
      }
      <Filter />
      <input
        type="file"
        accept="image/*"
        ref={inputRef}
        onChange={onInputChange}
        style={{ display: 'none' }}
      />
    </div>
  )
}