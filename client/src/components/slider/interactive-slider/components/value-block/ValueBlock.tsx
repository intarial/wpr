import styles from './ValueBlock.module.scss';
import {Radio} from "@ui/Radio/Radio.tsx";

type ValueType = {
    text: string
    type: 'radio' | 'checkbox'
    active: boolean
    update: () => void
}

export const ValueBlock = (value : ValueType ) => {

    return (
        <label onClick={ () => value.update() } className={ styles.wrapper } >
            <Radio active={ value.active } />
            <p className={ styles.text }>
                { value.text }
            </p>
        </label>
    )
}