import { useMemo, useState } from 'react';
import { ValueBlock } from '@components/slider/interactive-slider/components/value-block/ValueBlock.tsx';
import styles from './InteractiveSlider.module.scss';
import {TestQAArrayTypes} from "@packages/api/typings/test.ts";
import {SliderButtons} from "@ui/SliderButtons/SliderButtons.tsx";

export interface IContent {
  label: string
  values: string[]
  type: 'radio' | 'checkbox'
  description?: string
}

export interface IInteractive {
  content: IContent[]
  complete: (val: TestQAArrayTypes) => void
  nextButtonText?: string | JSX.Element
  prevButtonText?: string | JSX.Element
}

const InteractiveSlider = ({ interactive, closeTest }: { interactive: IInteractive, closeTest: () => void }): JSX.Element => {
  const [activePage, setActivePage] = useState<number>(1);
  const [collectedForm, setCollectedForm] = useState<TestQAArrayTypes>([]);
  const [activeValue, setActiveValue] = useState<number | undefined>();

  const countPages = interactive.content.length;
  const activeContent = useMemo(() => interactive?.content[activePage - 1], [
    activePage,
    interactive,
  ]);

  const updateActivePage = (type: 'plus' | 'minus'): void => {
    if (type === 'plus' && activePage + 1 <= countPages) {
      const updatedForm = [...collectedForm];
      if (activeContent) {
        updatedForm[activePage - 1] = {
          question: activeContent.label,
          answer: activeContent.values[activeValue ?? 0],
        };
      }
      setCollectedForm(updatedForm);
      setActivePage((prevActivePage) => prevActivePage + 1);
    } else if (type === 'plus' && activePage + 1 >= countPages) {
      interactive.complete(
        [
          ...collectedForm,
          { question: activeContent.label, answer: activeContent.values[activeValue ?? 0] }
        ]
      );
    }
    if (type === 'minus' && activePage - 1 !== 0) {
      setActivePage((prevActivePage) => prevActivePage - 1);
    } else if (type === 'minus') {
      closeTest()
    }
    setActiveValue(undefined);
  };

  return (
    <div className={styles.wrapper}>
      <SliderButtons
        activeValue={ activePage }
        countValue={ countPages }
        updateValue={ (type) => updateActivePage(type) }
      />
      <p className={styles.description}>{activeContent?.description}</p>
      <div className={styles.listValues}>
        {activeContent?.values.map((value, index) => (
          <ValueBlock key={index} text={value} active={ activeValue === index } type={ 'radio' } update={ () => setActiveValue(index) } />
        ))}
      </div>
    </div>
  );
};

export default InteractiveSlider;