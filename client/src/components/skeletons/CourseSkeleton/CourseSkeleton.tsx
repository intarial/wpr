import {Skeleton, SkeletonCircle, SkeletonText} from "@chakra-ui/react";
import {ReactNode} from "react";

const Link = ({ children, loaded }: { children: ReactNode, loaded: boolean }) => {
  return (
    <Skeleton
      isLoaded={ loaded }
      startColor='#F7F7F5' endColor='#E7E7E7'
      borderRadius={ 20 }
      boxSizing={ 'border-box' }
    >
      {
        children
      }
    </Skeleton>
  )
}

export const CourseSkeleton = { Link }