import styles from './ProfileUserSide.module.scss';
import {ImageFilter} from "@components/image/image-filter/ImageFilter.tsx";
import {IUser} from "@packages/api/typings/user.ts";
import {UserUtils} from "@utils/user";

export const ProfileUserSide = ({ user, change = false }: { user: IUser, change?: boolean }) => {

  return (
    <div className={ styles.wrapper } >
      <ImageFilter
        circle
        size={ 'x120' }
        change={ change }
        url={ user.avatar }
      />
      <div className={ styles.textContent }>
        <h3>
          { UserUtils.getNameInFirstLast(user) ?? 'Анонимный Пользователь' }
        </h3>
        <p>
          { user.email }
        </p>
      </div>
    </div>
  )
}