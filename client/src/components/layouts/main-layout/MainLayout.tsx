import { Header } from '@components/header/Header';
import { Footer } from '@components/footer/Footer';
import styles from './MainLayout.module.scss';
import {ReactNode} from "react";

interface IMainLayout {
  children: ReactNode,
  className?: string
  footer?: boolean
  fixedHeader?: boolean
}

export const MainLayout = ({ children, className, footer = true, fixedHeader = false } : IMainLayout) => {

  return (
    <div className={styles.wrapper}>
      <Header fixed={ fixedHeader } />
      <main className={[styles.layout, className, 'width-1274px'].join(' ')}>
        { children }
      </main>
      {
        footer && <Footer/>
      }
    </div>
  );
};

