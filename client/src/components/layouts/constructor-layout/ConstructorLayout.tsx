import { Header } from '@components/header/Header';
import { Footer } from '@components/footer/Footer';
import styles from './ConstructorLayout.module.scss';
import {ReactNode} from "react";

interface IConstructorLayout {
  children: ReactNode,
  className?: string
  footer?: boolean
  header?: boolean
  width1274?: boolean
  fixedHeader?: boolean
}

export const ConstructorLayout = ({ children, className, footer = true, fixedHeader = false, header = true, width1274 = true } : IConstructorLayout) => {

  return (
    <div className={styles.wrapper}>
      {
        header && <Header fixed={fixedHeader}/>
      }
      <main className={[styles.layout, className, width1274 && 'width-1274px'].join(' ')}>
        { children }
      </main>
      {
        footer && <Footer/>
      }
    </div>
  );
};

