import React, { ReactNode } from 'react';
import { NavLink } from 'react-router-dom';
import { ROUTE_NAMES } from '@routes/const.ts';
import { useAppSelector } from '@packages/store/hook.ts';
import {
  Menu,
  MenuButton,
  MenuList,
  MenuDivider,
  Flex,
} from '@chakra-ui/react';
import styles from './ProfileSide.module.scss';
import { IUser } from '@packages/api/typings/user.ts';
import { CustomAvatar } from '@ui/CustomAvatar/CustomAvatar.tsx';
import { Button } from '@ui/Button/Button.tsx';
import {Bell} from "lucide-react";
import {UserUtils} from "@utils/user";
import {serverUrl} from "@constants/browser.ts";

const LinkWrapper = ({ children, onClick }: { children: ReactNode, onClick?: () => void }) => {
  return <div onClick={ () => onClick && onClick() } className={styles.linkBlock}>{children}</div>;
};

const IsLoggedIn = ({ user }: { user: IUser }) => {
  const links = [
    { name: 'Настройки', link: `${ROUTE_NAMES.PROFILE_SETTINGS}` },
    { name: 'Активные курсы', link: `${ROUTE_NAMES.PROFILE}` },
    { name: 'Помощь', link: `${ROUTE_NAMES.PROFILE}` },
    { name: 'Контакты', link: `${ROUTE_NAMES.PROFILE}` },
  ];

  const handleLogout = () => {
    // TODO: handle logout logic here
  };

  return (
    <Flex zIndex={ 2 } alignItems="center">
      <Menu placement="bottom-end">
        <MenuButton
          rounded="full"
          cursor="pointer"
          aria-label="Menu"
        >
          {[user.avatar].map((url) => (
            <CustomAvatar
              key={url}
              aria-label="Profile avatar"
              className={styles.avatar}
              name={UserUtils.getNameInFirstLast(user)}
              url={`${serverUrl}/images/profile/${ url }`}
            />
          ))}
        </MenuButton>
        <MenuList className={styles.listMenu}>
          <NavLink to={ ROUTE_NAMES.PROFILE } className={styles.info}>
            {[user.avatar].map((url) => (
              <CustomAvatar
                key={url}
                className={styles.avatar}
                name={UserUtils.getNameInFirstLast(user)}
                url={`${serverUrl}/images/profile/${ url }`}
              />
            ))}
            <div className={styles.textContent}>
              <h5>
                { UserUtils.getNameInFirstLast(user) ?? 'Анонимный Пользователь' }
              </h5>
              <p>{user.email}</p>
            </div>
          </NavLink>

          <div className={styles.listLinks}>
            {links.map((link, index) => (
              <NavLink key={index} to={link.link} title={link.name}>
                <LinkWrapper aria-label={link.name}>
                  {link.name}
                </LinkWrapper>
              </NavLink>
            ))}
            <MenuDivider />
            <LinkWrapper onClick={handleLogout} aria-label="Logout">
              Выход
            </LinkWrapper>
          </div>
        </MenuList>
      </Menu>
    </Flex>
  );
};

export const ProfileSide: React.FC = () => {
  const { user } = useAppSelector((store) => store);
  const isLoggedIn = user.isLoginIn;
  const currentUser = user.user;

  return (
    <>
      <Button
        text={ !isLoggedIn ? 'Медиа' : undefined }
        icon={ isLoggedIn ? <Bell /> : undefined }
        size={ 'small' }
      />
      {isLoggedIn ? (
        <IsLoggedIn user={currentUser} />
      ) : (
        <NavLink
          state={{ type: 'login' }}
          to={{ pathname: ROUTE_NAMES.VALIDATION }}
        >
          <Button text="Личный кабинет" size="small" />
        </NavLink>
      )}
    </>
  );
};