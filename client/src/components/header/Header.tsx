import styles from './Header.module.scss';
import {IconLogo} from "@assets/icons";
import {Button} from "@ui/Button/Button";
import {NavLink} from "react-router-dom";
import {ROUTE_NAMES} from "@routes/const.ts";
import {ProfileSide} from "@components/header/components/ProfileSide/ProfileSide.tsx";

export const Header = ({ fixed } : { fixed?: boolean }) => {

  return (
    <div className={[ styles.layout, 'width-1274px', fixed && styles.fixed ].join(' ')} >
      <div className={ styles.wrapper } >
        <section className={ styles.leftContent }>
          <NavLink to={ ROUTE_NAMES.LANDING }>
            <IconLogo />
          </NavLink>
          <Button
            text={ 'Все курсы' }
            size={ 'small' }
            arrow={{ position: 'right' }}
          />
        </section>
        <section className={ styles.rightContent }>
          <ProfileSide />
        </section>
      </div>
    </div>
  )
}