import React from 'react';
import styles from './CourseTestModal.module.scss';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
} from '@chakra-ui/react'
import {Header} from "@components/header/Header.tsx";
import {useAppDispatch, useAppSelector} from "@packages/store/hook.ts";
import {setModalCourse} from "@packages/store/slices/modal/modal.ts";
import {Button} from "@ui/Button/Button.tsx";
import { X } from 'lucide-react'
import {NavLink} from "react-router-dom";

export const CourseTestModal: React.FC = () => {
  const modal = useAppSelector((store) => store.modal);
  const dispatch = useAppDispatch()

  const close = () => dispatch(setModalCourse({ type: 'test', value: false}))

  return (
    <Modal onClose={close} isOpen={modal.course.test} isCentered>
      <ModalOverlay />
      <ModalContent className={ styles.wrapper }>
        <ModalBody
          className={[ 'width-1274px', styles.content ].join(' ')}
        >
          <Header fixed />
          <div className={ styles.modalInfo }>
            <h3>
              Поможем определить направление
            </h3>
            <p className={ styles.description }>
              Пройдите тест на профориентацию, и мы подберём вам подходящие курсы и профессии
            </p>
            <div className={ styles.buttonContent }>
              <NavLink to={ '/course-test' }>
                <Button
                  text='Пройти тест'
                  size='medium'
                  color='blue'
                />
              </NavLink>
              <div
                onClick={ close }
                className={ styles.close }
              >
                <X />
              </div>
            </div>
          </div>
        </ModalBody>
      </ModalContent>
    </Modal>
  )
}

