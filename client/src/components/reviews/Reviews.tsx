import React, {useEffect, useState} from 'react';
import styles from './Reviews.module.scss';
import {useAppSelector} from "@packages/store/hook.ts";
import {CustomSwiper} from "@ui/CustomSwiper/CustomSwiper.tsx";
import {IReview} from "@packages/api/typings/review.ts";
import {IUser} from "@packages/api/typings/user.ts";
import {CustomAvatar} from "@ui/CustomAvatar/CustomAvatar.tsx";
import {ICourseItem} from "@packages/api/typings/courses.ts";
import {Button} from "@ui/Button/Button.tsx";
import {
  PlayCircle,
  StickyNote
} from 'lucide-react'
import api from "@packages/api";
import {UserUtils} from "@utils/user";

const ReviewBlock = ({ item }: { item: IReview }) => {
  const [user, setUser] = useState<IUser>()
  const [course] = useState<ICourseItem>()

  useEffect(() => {
    api.userApi.getUserById(item.user_id).then(
      res => {
        setUser(res.data.user)

        if (!res.data.user.courses?.length) return

        // api.courseApi.getCourseById(res.data.user.course_id).then(
        //   res => {
        //     setCourse(res.data.result)
        //   }
        // )
      }
    )
  }, [item.user_id])

  if (!user) return null

  return (
    <div className={ styles.reviewBlock }>
      <div className={ styles.userSide }>
        <div className={ styles.avatar }>
          <CustomAvatar
            size={ 'full' }
            name={ UserUtils.getNameInFirstLast(user) ?? 'Анонимный пользователь' }
            url={ user.avatar }
          />
        </div>
        <div className={ styles.textContent }>
          <h3>
            { UserUtils.getNameInFirstLast(user) ?? 'Анонимный пользователь' }
          </h3>
          <p>{ course?.name }</p>
        </div>
      </div>
      <p className={ styles.description }>
        { item.text }
      </p>
      {
        (item.video || item.portfolio) && (
          <div className={styles.listRescources}>
            {
              item.video && (
                <Button
                  color={'grey'} size={'small'}
                  icon={<PlayCircle/>}
                  text={'Видеоотзыв'}
                />
              )
            }
            {
              item.portfolio && (
                <Button
                  color={'grey'} size={'small'}
                  icon={<StickyNote/>}
                  text={'Портфолио'}
                />
              )
            }
          </div>
        )
      }
    </div>
  )
}

export const Reviews = () => {
  const {
    review: {
      items
    }
  } = useAppSelector((store) => store);

  return (
    <section className={ styles.wrapper } >
      <div className={[ styles.listReviews, 'width-1274px' ].join(' ')} >
        <h4 className={ styles.title }>
          Что говорят студенты школы
        </h4>
        <CustomSwiper
          items={ items }
          slidesPerView={ 2 }
          render={(item: IReview) => (
            <ReviewBlock  item={ item } />
          )}
        />
      </div>
    </section>
  )
}