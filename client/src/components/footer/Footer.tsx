import styles from './Footer.module.scss';
import {IconLogo} from "@assets/icons";
import {Button} from "@ui/Button/Button.tsx";
import {socials} from "@constants/socials.tsx";
import {NavLink} from "react-router-dom";
import {navigation} from "@constants/navigation.tsx";
import {CustomAvatar} from "@ui/CustomAvatar/CustomAvatar.tsx";

const FooterNavigationBlock = ({ nav }: { nav: { title: string, path: string } }) => {
  return (
    <li>
      <NavLink to={ nav.path }>
        { nav.title }
      </NavLink>
    </li>
  )
}

export const Footer = () => {

  return (
    <div className={[ styles.layout, 'width-1274px' ].join(' ')} >
      <div className={ styles.wrapper } >
        <header className={ styles.header }>
          <IconLogo />
          <div className={ styles.socials }>
            {
              socials.map(
                social => (
                  <Button
                    key={ social.name.en }
                    size={ 'small' }
                    icon={ social.icon }
                    tooltip={ social.name.ru }
                    color={ 'white' }
                  />
                )
              )
            }
          </div>
        </header>
        <main className={ styles.main }>
          <nav className={ styles.listPaths }>
            {
              navigation.footer.map(
                nav => (
                  <ul key={ nav.title }>
                    <h3 className={ styles.titleNav }>
                      { nav.title }
                    </h3>
                    {
                      nav.urls.map(
                        url => (
                          <FooterNavigationBlock
                            key={ url.path }
                            nav={ url }
                          />
                        )
                      )
                    }
                  </ul>
                )
              )
            }
          </nav>
          <div className={ styles.contacts }>
            <div className={ styles.managerContact }>
              <div className={ styles.managerContactHeader }>
                <CustomAvatar size={ 'sm' } />
                <h3>
                  Есть вопрос или предложение?
                </h3>
              </div>
              <div className={ styles.managerContactLink }>
                <a>Напишите менеджеру</a> - ответственному за качество курсов
              </div>
            </div>
            <div className={ styles.mainContacts }>
              <p className={ styles.phone }>
                +7 (999) 123-00-43
              </p>
              <p className={ styles.contactCenter }>
                Контактный центр
              </p>
            </div>
          </div>
        </main>
        <footer className={ styles.footer }>
          <p className={ styles.copyright }>
            ©2023, Codeology
          </p>
          <nav className={ styles.navigation }>
            <ul>
              {
                navigation.documents.map(
                  nav => (
                    <li key={ nav.path }>
                      <a>
                        { nav.title }
                      </a>
                    </li>
                  )
                )
              }
            </ul>
          </nav>
        </footer>
      </div>
    </div>
  )
}